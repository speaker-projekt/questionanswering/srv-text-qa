{{short description|Geographical features of Italy}}
{{Infobox country geography
| name = Italy
| map = Satellite image of Italy in March 2003.jpg
| continent = [[Europe]] and [[Africa]] (Lampedusa and Lampione islands)
| region = [[South Europe]]
| coordinates = {{coord|42|00|N|12|05|E|type:country|display=inline,title}}
| km area = 301,339
| percent land = 97.61
| km coastline = 7,600
| borders = [[Land borders|Total land borders]] <br/> 1836.4 km
| highest point = [[Mont Blanc]] <br/> 4,810 m
| lowest point = [[Jolanda di Savoia]] <br/> -3.44 m
| longest river = [[Po River|Po]] <br/> 652 km
| largest lake = [[Lake Garda|Garda]] <br/> {{convert|370|km2|mi2|abbr=on}}
| exclusive economic zone = {{convert|541,915|km2|mi2|abbr=on}}
}}
[[File:Koppen-Geiger Map ITA present.svg|thumb|[[Köppen climate classification]] types of Italy]]
'''[[Italy]]''' is located in southern [[Europe]] and comprises the long, boot-shaped [[Italian Peninsula]], the southern side of Alps, the large plain of the [[Po Valley]] and some islands including [[Sicily]] and [[Sardinia]]. [[Corsica]], although belonging to the Italian geographical region, has been a part of France since 1769. Italy is part of the Northern Hemisphere. Two of the Pelagie islands (Lampedusa and Lampione) are located in the African continent. 

Its total area is {{convert|301,340|km2|abbr=on}}, of which {{convert|294,140|km2|abbr=on}} is land and {{convert|7,200|km2|0|abbr=on|adj=mid|is water}}. It lies between latitudes [[35th parallel north|35°]] and [[48th parallel north|48° N]], and longitudes [[6th meridian east|6°]] and [[19th meridian east|19° E]].

Italy borders [[Switzerland]] ({{convert|698|km|disp=or|abbr=on}}), [[France]] ({{convert|476|km|disp=or|abbr=on}}), [[Austria]] ({{convert|404|km|disp=or|abbr=on}}) and [[Slovenia]] ({{convert|218|km|disp=or|abbr=on}}). [[San Marino]] ({{convert|37|km|disp=or|abbr=on}}) and [[Vatican city]] ({{convert|3.4|km|disp=or|abbr=on}}) are [[enclave]]s. The total border length is {{convert|1836.4|km|abbr=on}}.

Including islands, Italy has a coastline of {{convert|7,600|km|abbr=on}} on the [[Adriatic Sea]], [[Ionian Sea]], [[Tyrrhenian Sea]], [[Ligurian Sea]], [[Sea of Sardinia]] and [[Strait of Sicily]].

==Mountains and plains==
[[File:Mont Blanc from Aosta Valley.JPG|thumb|Mont Blanc seen from [[Aosta Valley]].]]
[[File:Italy topographic map-blank.svg|thumb|Topographic map of Italy]]
Almost 40% of the Italian territory is mountainous,<ref name=eug92>{{cite book|last=Riganti]|first=[dir. da Alberto|title=Enciclopedia universale Garzanti.|year=1991|publisher=Garzanti|location=Milano|isbn=88-11-50459-7|edition=Nuova ed. aggiornata e ampliata.}}</ref> with the [[Alps]] as the northern boundary and the [[Apennine Mountains]] forming the backbone of the peninsula and extending for {{convert|1350|km|abbr=on}}.<ref name=eug92 /> In between the two lies a [[Po Valley|large plain]] in the valley of the [[Po River|Po]], the largest river in Italy, which flows {{convert|652|km|mi|0|abbr=on}} eastward from the [[Cottian Alps]] to the Adriatic. The Po Valley is the largest plain in Italy, with {{convert|46,000|km2|abbr=on}}, and it represents over 70% of the total plain area in the country.<ref name=eug92 />

The Alpine mountain range is linked with the Apennines with the [[Colle di Cadibona]] pass in the [[Ligurian Alps]].

Worldwide-known mountains in Italy are Monte Cervino ([[Matterhorn]]), [[Monte Rosa]], [[Gran Paradiso]] in the West Alps, and [[Bernina Range|Bernina]], [[Stelvio Pass|Stelvio]] and [[Dolomites]] along the eastern side of the Alps. The highest peak in Italy is [[Mont Blanc]], at {{convert|4810|meters}} [[Above mean sea level|above sea level]].

===Volcanoes===
Many elements of the Italian territory are of volcanic origin. Most of the small islands and [[archipelago]]s in the south, like [[Capraia]], [[Ponza]], [[Ischia]], [[Eolie]], [[Ustica]] and [[Pantelleria]] are [[volcanic island]]s.
There are also active volcanoes: [[Mount Etna|Etna]], in Sicily, the largest active volcano in Europe; [[Vulcano]], [[Stromboli]], and [[Vesuvius]], near [[Naples]], the only active volcano on mainland Europe.

==Rivers and seas==
Most of Italy's [[river]]s drain either into the Adriatic Sea (like Po, [[Piave (river)|Piave]], [[Adige]], [[Brenta (river)|Brenta]], [[Tagliamento]], [[Reno River|Reno]]) or into the Tyrrhenian (like [[Arno]], [[Tiber]] and [[Volturno]]), though the waters from some border municipalities ([[Livigno]] in [[Lombardy]], [[Innichen]] and [[Sexten]] in [[Trentino-Alto Adige/Südtirol]], [[Tarvisio]] in [[Friuli-Venezia-Giulia]]) drain into the [[Black Sea]] through the basin of the [[Drava]], a [[tributary]] of the [[Danube]], and the waters from the [[Lago di Lei]] in Lombardy drain into the [[North Sea]] through the basin of the [[Rhine]].

===Maritime claims===
* Territorial sea: {{convert|12|nmi|km mi|1|abbr=on}}
* Continental shelf: {{convert|200|m|ft|adj=mid|depth}} or to the depth of exploitation
* [[Exclusive Economic Zone]]: {{convert|541,915|km2|mi2|abbr=on}}

==Lakes==
[[File:Altstadt von Malcesine-2.jpg|thumb|[[Lake Garda]] is the largest of the [[List of lakes in Italy|Italian lakes]].]]
In the north of the country are a number of subalpine [[moraine-dammed lake|moraine-dammed]] lakes (the [[Italian Lakes]]), the largest of which is [[Lake Garda|Garda]] ({{convert|370|km2|sqmi|0|abbr=on|disp=or}}). Other well known of these subalpine lakes are [[Lake Maggiore]] ({{convert|212.5|km2|sqmi|0|abbr=on|disp=or}}), whose most northerly section is part of Switzerland, [[Como lake|Como]] ({{convert|146|km2|sqmi|0|abbr=on|disp=or}}), [[Orta lake|Orta]], [[Lugano lake|Lugano]], [[Iseo lake|Iseo]], [[Idro lake|Idro]].

Other notable lakes in the Italian peninsula are [[Trasimeno]], [[Lake Bolsena|Bolsena]], [[Lake Bracciano|Bracciano]], [[Lake Vico|Vico]], [[Lago di Varano|Varano]] and [[Lake Lesina|Lesina]] in [[Gargano]] and [[Lake Omodeo|Omodeo]] in [[Sardinia]].

==Islands==
Italy includes several islands. The largest are Sicily {{convert|25708|km2|abbr=on}} and Sardinia {{convert|24090|km2|abbr=on}}. The third largest island is [[Elba]], the largest island of the [[Tuscan Archipelago]] ({{convert|224|km2|sqmi|0|abbr=on|disp=or}}).

===Latitude and longitude===
* Northernmost point &mdash; [[Testa Gemella Occidentale]], [[Prettau]] (Predoi), [[South Tyrol]] at {{coord|47|5|N|12|11|E|type:landmark_region:IT_source:frwiki|name=North: Testa Gemella Occidentale}}
* Southernmost point &mdash; [[Punta Pesce Spada]], [[Lampedusa]], [[Sicily]] at {{coord|35|29|N|12|36|E|type:landmark_region:IT_source:frwiki|name=South: Lampedusa}} (whole territory); [[Capo Spartivento]], [[Palizzi]], [[Calabria]] at {{coord|37|55|N|15|59|E|region:IT_type:landmark|name=South: Capo Spartivento}} (mainland)
* Westernmost point &mdash; [[Rocca Bernauda]], [[Bardonecchia]], [[Piedmont]] at {{coord|45|6|N|6|37|E|region:IT_type:landmark_source:frwiki|name=West: Rocca Bernauda}}
* Easternmost point &mdash; [[Capo d'Otranto]], [[Otranto]], [[Apulia]] at {{coord|40|6|N|18|31|E|region:IT_type:landmark_source:frwiki|name=East: Capo d'Otranto}}

===Elevation===
* Highest: [[Mont Blanc]], [[Courmayeur]] (4807.5 m) at {{coord|45|50|N|6|51|E|region:IT_type:mountain_source:frwiki|name=Mont Blanc (highest)}}
* Lowest: [[Jolanda di Savoia]] (-3.44m) at {{coord|44|53|N|11|59|E|region:IT_type:landmark|name=Le Contane (lowest)}}
* Highest settlement: [[Trepalle]], [[Livigno]] (2,209 m) at {{coord|46|32|N|10|11|E|region:IT|name=Trepalle }}

==Land use==
<ref name=landuse>{{cite web|title=Analisi dei cambiamenti della copertura ed uso del suolo in Italia nel periodo 2000-2006 |url=http://www.sinanet.isprambiente.it/it/coperturasuolo/analisi-cambiamenti-lulc-in-italia-nel-periodo-2000-2006.pdf |publisher=ISPRA |access-date=23 November 2011 |url-status=dead |archive-url=https://web.archive.org/web/20120426001120/http://www.sinanet.isprambiente.it/it/coperturasuolo/analisi-cambiamenti-lulc-in-italia-nel-periodo-2000-2006.pdf |archive-date=26 April 2012 }}</ref><ref group=Note>2006 estimates.</ref>
* Artificial (urban, industrial etc.): 4.9%
* Agricultural: 52.2%
** Arable land: 27.9%
** Permanent: 7.1%
** Other: 17.2%
* Wood: 41.4%
* Wetlands: 0.4%
* Water (lakes etc.): 1.1%

===Irrigated land===
* 39,510&nbsp;km<sup>2</sup> (2007)

===Total renewable water resources===
* 191.3&nbsp;km<sup>3</sup> (2011)

===Freshwater withdrawal (domestic/industrial/agricultural)===
*''total'': 45.41&nbsp;km<sup>3</sup>/yr (24%/43%/34%)
*''per capita:'' 789.8 m<sup>3</sup>/yr (2008)

==Gallery==
<gallery>
File:ItalyOMC.png|Italy's cities and main towns.
File:Southern Italian Peninsula at Night.JPG|Astronaut photograph highlighting the night-time appearance of southern Italy.
File:ItalySouth1849.jpg|[[Southern Italy]] and Sicily on the 1849 map.
File:EtnaAvió.JPG|Etna, the highest active volcano in Europe.
File:Torre della pelosa + isola piana + asinara da capo falcone.jpg|[[Asinara]] island with the Aragonese Torre della Pelosa (16th century), Sardinia.
File:Vernazza.JPG|The coastal areas of [[Liguria]] have a Mediterranean climate.
File:San Quirico d'Orcia - Chiesetta Val d'Orcia.jpg|Landscape of [[Tuscany]].
File:RisaieVercellesi_Panorama2.jpg|Rice paddies in the [[Po Valley]] near [[Vercelli]].
File:Cascatemarmore.jpg|[[Cascata delle Marmore|Marmore waterfall]], the world's tallest man-made waterfall, was created by the ancient Romans.
File:Vesuvius_from_Pompeii_%28hires_version_2_scaled%29.png|Mount Vesuvius looms over the ruins of [[Pompeii]].
File:Venice_as_seen_from_the_air_with_bridge_to_mainland.jpg|Panorama of [[Venice]] and its lagoon.
File:Rilke_05.jpg|The [[Karst Plateau]] drops vertically into the Adriatic Sea near [[Trieste]].
File:Gran_sasso_italia.jpg|[[Gran Sasso d'Italia]], the highest peak of the [[Apennines]].
File:Monviso_from_San_Marzano_Oliveto.jpg|Vineyards in the [[Montferrat]] hills, with the [[Monviso]] in the background.
File:Panoramic_Livigno.jpg|Livigno, the tallest 'comune' of Italy, during winter.
File:Reggio_calabria_panorama_dal_fortino.jpg|The [[Strait of Messina]] as seen from mainland.
File:Filicudi_%288_of_28%29.jpg|'La Canna' rock off the coast of [[Filicudi]].
File:Chia beach, Sardinia, Italy.jpg|Southern coast of Sardinia.
File:Cremona_Po_Bridge.jpg|The Po river at Cremona, in a foggy winter day.
</gallery>

== See also ==
{{GeoGroupTemplate}}
* [[Climate of Italy]]
* [[List of islands of Italy]]
* [[Natural hazards in Italy]]

==Notes==
{{reflist|group=Note}}

==References==
{{reflist}}{{CIA World Factbook}}{{Italy topics}}
{{Geography of Europe}}
{{Extreme points of Europe}}

{{DEFAULTSORT:Geography Of Italy}}
[[Category:Geography of Italy| ]]
[[Category:Geology of Italy]]