{{Infobox Christian denomination
|name                = Protestant Church in the Netherlands
|image               = Protestant Church in the Netherlands.svg
|caption             =
|main_classification = [[Protestantism|Protestant]]
|orientation         = [[Calvinism|Reformed]] and [[Lutheranism|Lutheran]]
|polity              = Mixture of [[Presbyterian polity|Presbyterian]] and [[Congregationalist polity|Congregationalist]]
|founded_date        = 1 May 2004
|founded_place       = Netherlands
|merger              = {{plainlist|
*[[Dutch Reformed Church]]
*[[Reformed Churches in the Netherlands]]
*[[Evangelical Lutheran Church in the Kingdom of the Netherlands]]}}
|associations        = {{plainlist|
*[[Conference of European Churches]]
*[[World Communion of Reformed Churches]]
*[[Lutheran World Federation]]
*[[World Council of Churches]]}}
|separations         = {{plainlist|
*[[Restored Reformed Church]]
*[[Continued Reformed Churches in the Netherlands]]}}(newly organized denominations; refused to participate in the merger)
|congregations       = c. 2,000
|members             = 1.6 million (9.1% of the population)<ref name="PKN Statistische Jaarbrief 2017">[https://www.protestantsekerk.nl/download/CAwdEAwUUkRBXEU=&type=pdf PKN Statistische Jaarbrief 2017]</ref>
|website             = {{Official URL}}
|footnotes           = 
}}

The '''Protestant Church in the Netherlands''' ({{lang-nl|de Protestantse Kerk in Nederland}},  abbreviated '''PKN''') is the largest [[Protestant]] denomination in the Netherlands, being both [[Reformed]] (Calvinist) and [[Lutheran]].

It was founded on 1 May 2004 as the [[United and uniting churches|merger]] of the vast majority of  [[Dutch Reformed Church]], the vast majority of the [[Reformed Churches in the Netherlands]] and the [[Evangelical Lutheran Church in the Kingdom of the Netherlands]].<ref name=3wayPKN>GoDutch.com, [http://www.godutch.com/newspaper/index.php?id=571 "Three-way PKN Union Drastically Changes Dutch Denominational Landscape: Two Groups of Merger Opponents Stay Out"], May 24, 2004. Accessed July 13, 2010.</ref><ref>[https://www.trouw.nl/home/strijd-met-de-onkerk-~addb2469/ strijd met de onkerk ]</ref>  The merger was the culmination of an organizational process started in 1961. Several orthodox Reformed and liberal churches did not merge into the new church.

The Protestant Church in the Netherlands (PKN) forms the country's second largest [[Christian denomination]] after the [[Roman Catholic Church]], with approximately 1.6 million members as per the church official statistics or some 9.1% of the population in 2016.<ref name="PKN Statistische Jaarbrief 2017" /> It is the traditional faith of the [[Dutch Royal Family]] &ndash; a remnant of  historical dominance of the [[Dutch Reformed Church]], the main predecessor of the Protestant Church.

==Doctrine and practice==
[[File:Westerkerk Amsterdam 20041002.jpg|thumb|left|[[Westerkerk]] in [[Amsterdam]]]]
The doctrine of the Protestant Church in the Netherlands is expressed in its [[creed]]s. In addition to holding the [[Apostles' Creed|Apostles']], the [[Nicene Creed|Nicene]] and the [[Athanasian Creed]]s of the [[Christian Church|universal church]], it also holds to the confessions of its predecessor bodies. From the Lutheran tradition are the unaltered [[Augsburg Confession]] and [[Luther's Large Catechism|Luther's Catechism]]. From the Reformed, the [[Heidelberg Catechism|Heidelberg]] and [[Catechism#Genevan Catechism|Genevan Catechisms]] along with the [[Belgic Confession]] with the [[Canons of Dordt]]. The Church also acknowledges the [[Theological Declaration of Barmen]] and the [[Leuenberg Agreement]].<ref name=churchorderp1>[http://www.protestantchurch.nl/site/uploadedDocs/CHURCH_ORDER.pdf Church Order of the Protestant Church in the Netherlands] {{Webarchive|url=https://web.archive.org/web/20110719162608/http://www.protestantchurch.nl/site/uploadedDocs/CHURCH_ORDER.pdf |date=2011-07-19 }}. Article I, p. 1. Accessed July 13, 2010.</ref> [[Ordination of women]] and [[Blessing of same-sex unions in Christian churches|blessings of same-sex marriages]] are allowed.<ref>[https://www.christianpost.com/news/protestant-church-in-netherlands-to-grant-blessings-to-gay-couples.html Christian Post: Protestant Church in Netherlands to Grant ''Blessings'' to Gay Couples], 2007</ref>

The PKN contains both liberal and conservative movements; although the liberal [[Remonstrants]] left talks when they could not agree with the unaltered adoption of the Canons of Dordt. Local congregations have far-reaching powers concerning "controversial" matters (such as admittance to [[holy communion]] or whether women are admitted as members of the congregation's [[Session (Presbyterianism)|consistory]]).

==Organization==
The polity of the Protestant Church in the Netherlands is a hybrid of [[Presbyterian polity|presbyterian]] and [[Congregationalist polity|congregationalist]] church governance. Church governance is organised along local, regional, and national lines. At the local level is the congregation. An individual congregation is led by a church council made of the minister along with [[Elder (Christianity)|elders]] and [[deacon]]s elected by the congregation. At the regional level were 75 classical assemblies whose members are chosen by the church councils. As of May 1, 2018, these 75 classical assemblies are reorganized into 11 larger ones. At the national level is the General [[Synod]] which directs areas of common interest, such as theological education, ministry training and ecumenical cooperation.<ref name=organisation>[http://www.protestantchurch.nl/info.aspx?page=1509# Organisation of the PKN] {{Webarchive|url=https://web.archive.org/web/20110719162652/http://www.protestantchurch.nl/info.aspx?page=1509 |date=2011-07-19 }}. Accessed July 14, 2010.</ref>

The PKN has four different types of congregations:
# Protestant congregations: local congregations from different church bodies that have merged
# Dutch Reformed congregations
# Reformed congregations (congregations of the former Reformed Churches in the Netherlands)
# Lutheran congregations (congregations of the former Evangelical-Lutheran Church)

Lutherans are a minority (about 1 percent) of the PKN's membership. To ensure that Lutherans are represented in the Church, the Lutheran congregations have their own synod. The Lutheran Synod also has representatives in the General Synod.<ref name=organisation/>

==Statistical details==
The Protestant Church in the Netherlands issues yearly reports regarding its membership and finances.<ref name="PKN Statistische Jaarbrief 2017">[https://www.protestantsekerk.nl/download/CAwdEAwUUkRBXEU=&type=pdf PKN Statistische Jaarbrief 2017]</ref>

Its make-up by former affiliation of its congregations was as follows in 2017:

{| class="wikitable sortable" font-size:80%;"
|+ style="font-size:100%" |Members of the Protestant Church in the Netherlands by type of congregation (2017)<ref name="PKN Statistische Jaarbrief 2017">[https://www.protestantsekerk.nl/download/CAwdEAwUUkRBXEU=&type=pdf PKN Statistische Jaarbrief 2017]</ref>
|-
! Former affiliation of the congregation
! colspan="100"|% of members in the Protestant Church in the Netherlands
|-
| no former affiliation, merged, or simply identifying as [[Protestant]] (''Protestants'')
|align=right| {{bartable|57.3||2||background:mediumblue}}
|-
| former [[Dutch Reformed Church]] (''Hervormd'')
|align=right| {{bartable|33.6||2||background:mediumblue}}
|-
| former [[Reformed Churches in the Netherlands]] (''Gereformeerd'')
|align=right| {{bartable|8.6||2||background:mediumblue}}
|-
| former [[Evangelical Lutheran Church in the Kingdom of the Netherlands]] (''Luthers'')
|align=right| {{bartable|0.5||2||background:mediumblue}}
|}

Trend shows that since 2011 identification with former denominations has been falling in favor of simply identifying as "Protestant".

==Secularization==
Secularization, or the decline in religiosity, first became noticeable after 1960 in the Protestant rural areas of [[Friesland]] and [[Groningen]]. Then, it spread to [[Amsterdam]], [[Rotterdam]] and the other large cities in the west. Finally the Catholic southern areas showed religious declines. A countervailing trend is produced by a religious revival in the Dutch [[Bible Belt (Netherlands)|Bible Belt]], and the growth of Muslims and Hindu communities resulting from immigration and high birth rates.<ref name="Knippenberg1998">{{cite journal|last1=Knippenberg|first1=Hans|journal=GeoJournal|volume=45|issue=3|year=1998|pages=209–220|issn=03432521|doi=10.1023/A:1006973011455|title=Secularization in the Netherlands in its historical and geographical dimensions
}}</ref><ref>{{citation|first1=Tomáš|last1= Sobotka|first2= Feray|last2= Adigüzel|title=Religiosity and spatial demographic differences in the Netherlands|date=2002|hdl=11385/169984 }}</ref>
Research in 2007 concluded that 42% of the members of the PKN were [[non-theist]]s.<ref>{{cite book|title=God in Nederland (1996-2006)|first1=Ronald |last1=Meester|first2= G.|last2= Dekker|isbn=9789025957407}}</ref> Furthermore, in the PKN and several other smaller denominations of the Netherlands, one in six clergy were either agnostic or atheist.<ref name=PKN-Hendrikse>{{cite news|last=Pigott|first=Robert|title=Dutch rethink Christianity for a doubtful world|url=https://www.bbc.co.uk/news/world-europe-14417362|access-date=2 October 2011|newspaper=BBC News|date=5 August 2011}}</ref><ref>{{Youtube|id=n6TuZ9F-PGo}}</ref> A minister of the PKN, [[Klaas Hendrikse]] once described God as "a word for experience, or human experience" and said that Jesus may have never existed.<ref name="PKN-Hendrikse" /><ref>{{Youtube|id=9eypysiJQgw}}</ref>

==Separations==
[[File:Ontstaansgeschiedenis van kerken in Nederland.jpg|thumb|right|History of the churches in the  Netherlands]]
Only those congregations belonging to the former Reformed Churches in the Netherlands have the legal right to secede from the PKN without losing its property and church during a transition period of 10 years. Seven congregations have so far decided to form the [[Continued Reformed Churches in the Netherlands]].<ref name=3wayPKN/> Two congregations have joined one of the other smaller Reformed churches in the Netherlands. Some minorities within congregations that joined the PKN decided to leave the church and associated themselves individually with one of the other Reformed churches.

Some congregations and members in the Dutch Reformed Church did not agree with the merger and have separated. They have organized themselves in the [[Restored Reformed Church]]. Estimations of their membership vary from 35,000 up to 70,000 people in about 120 local congregations.<ref>{{Cite web |url=http://www.hersteldhervormdekerk.nl/website/gemeenten |title=Official website Restored Reformed Church |access-date=2011-09-06 |archive-url=https://web.archive.org/web/20101023230643/http://www.hersteldhervormdekerk.nl/website/gemeenten |archive-date=2010-10-23 |url-status=dead }}</ref> They disagree with the pluralism of the merged church which maintains, as they see it, contradicting Reformed and Lutheran confessions. This group also considers same-sex marriages and female clergy unbiblical.

==Involvement in the Middle East==
In a meeting of eight Jewish and eight Protestant Dutch leaders in Israel in May 2011, a statement of cooperation was issued, indicating, for the most part, that the Protestant Church recognizes the issues involved with the Palestinian Christians and that this is sometimes at odds with support for the [[State of Israel]], but standing up for the rights of the [[State of Palestine|Palestinians]] does not detract from the emphasis on the safety of the State of Israel and vice versa.<ref>{{Cite web |url=http://www.protestantchurch.nl/info.aspx?page=16308 |title=Encounter and dialogue |access-date=2011-08-14 |archive-url=https://web.archive.org/web/20111118110306/http://www.protestantchurch.nl/info.aspx?page=16308 |archive-date=2011-11-18 |url-status=dead }}</ref>

==See also==
* [[Bible Belt (Netherlands)]]
* [[History of religion in the Netherlands]]
* [[United and uniting churches]]
* [[Religion in the Netherlands]]

== References ==
{{Reflist}}

==External links==
*{{Official website}} (in Dutch)

{{Lutheran World Federation Churches}}

{{DEFAULTSORT:Protestant Church In The Netherlands}}
[[Category:Protestant Church Christians from the Netherlands| ]]
[[Category:Calvinism in the Netherlands]]
[[Category:Christian denominations in the Netherlands]]
[[Category:Dutch Reformed Church]]
[[Category:Lutheran World Federation members]]
[[Category:Lutheranism in the Netherlands]]
[[Category:Protestant denominations established in the 21st century]]
[[Category:Reformed denominations in the Netherlands]]
[[Category:National churches|Netherlands]]
[[Category:United and uniting churches]]
[[Category:Christian organizations established in 2004]]
[[Category:2004 establishments in the Netherlands]]