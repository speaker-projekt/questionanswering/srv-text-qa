
{{short description|American film and television director (1930–2002)}}
{{Use mdy dates|date=November 2020}}
{{Infobox person
| name = John Frankenheimer
| image = Frankenheimer - Life - portrait.jpg
| birth_name = John Michael Frankenheimer
| birth_date = {{Birth date|1930|2|19}}
| birth_place = Queens, New York City, U.S.
| death_date = {{Death date and age|2002|7|6|1930|2|19}}
| death_place = Los Angeles, California, U.S.
| years_active = 1948–2002
| alma_mater = Williams College
| occupation = Film director
| spouse = Joanne Frankenheimer (divorced)<br>{{marriage|Carolyn Miller|1954|1962|end=divorced}}<br>{{marriage|Evans Evans|1963}}
| children = 2 (with Miller)

}}
John Michael Frankenheimer (February 19, 1930 – July 6, 2002) was an American film and television director known for social dramas and action/suspense films. Among his credits were Birdman of Alcatraz (1962), The Manchurian Candidate (1962), Seven Days in May (1964), The Train (1964), Seconds (1966), Grand Prix (1966), French Connection II (1975), Black Sunday (1977), Ronin (1998), and Reindeer Games (2000).

He won four Emmy Awards—three consecutive—in the 1990s for directing the television movies Against the Wall, The Burning Season, Andersonville, and George Wallace, the last of which also received a Golden Globe Award for Best Miniseries or Television Film. 

Frankenheimer's 30 feature films and over 50 plays for television were notable for their influence on contemporary thought. He became a pioneer of the "modern-day political thriller", having begun his career at the height of the Cold War.

He was technically highly accomplished from his days in live television; many of his films were noted for creating "psychological dilemmas" for his male protagonists along with having a strong "sense of environment," similar in style to films by director Sidney Lumet, for whom he had earlier worked as assistant director. He developed a "tremendous propensity for exploring political situations" which would ensnare his characters.

Movie critic Leonard Maltin writes that "in his time [1960s]... Frankenheimer worked with the top writers, producers and actors in a series of films that dealt with issues that were just on top of the moment&mdash;things that were facing us all."

==Early life==
Frankenheimer was born in Queens, New York City, the son of Helen Mary (née Sheedy) and Walter Martin Frankenheimer, a stockbroker. Frankenheimer once speculated he might be related to actress Ally Sheedy. His father was of German Jewish descent, his mother was Irish Catholic, and Frankenheimer was raised in his mother's religion.

He grew up in New York City and became interested in movies at an early age; he recalled going to the cinema every weekend. In 1947, he graduated from La Salle Military Academy in Oakdale, Long Island, New York. In 1951, he graduated from Williams College in Williamstown, Massachusetts, where he had studied English. He also developed an interest in acting as a career while in college but began thinking seriously about directing when he was in the Air Force.

This led him to join a film squadron based in Burbank, California, where he shot his first documentary. He also began studying film theory by reading books about other famous directors, such as Sergei Eisenstein along with how-to books about the craft of film making.

==Career==
Frankenheimer began his directing career in live television at CBS. Throughout the 1950s he directed over 140 episodes of shows like Playhouse 90, Climax!, and Danger, including The Comedian, written by Rod Serling and starring Mickey Rooney as a ragingly vicious television comedian.

Frankenheimer's first theatrical film was The Young Stranger (1957), starring James MacArthur as the rebellious teenage son of a powerful Hollywood movie producer. He directed the production, based on a Climax! episode, "Deal a Blow", which he directed when he was 26. Frankenheimer returned to television during the late 1950s, moving to film permanently in 1961 with The Young Savages, in which he worked for the first time with Burt Lancaster in a story of a young boy murdered by a New York gang. His departure from television is considered to signal the end of the Golden Age of Television.

Roger Ebert considered Frankenheimer to have had a special gift as a filmmaker and to have been a "master craftsman". He stated that Frankenheimer made some of the "most distinctive films of his time" and that he was " one of the most gifted directors of drama on television".

===Birdman of Alcatraz===
Production of Birdman of Alcatraz began under director Charles Crichton. Burt Lancaster, who was producing, as well as starring, asked Frankenheimer to take over the film. As Frankenheimer describes in Charles Champlin's interview book, he advised Lancaster that the script was too long, but was told he had to shoot all that was written.

The first cut of the film was four-and-a-half hours long, the length Frankenheimer had predicted. Moreover, the film was constructed so that it could not be cut and still be coherent. Frankenheimer said the film would have to be rewritten and partly reshot. Lancaster was committed to star in Judgment at Nuremberg, so he made that film while Frankenheimer prepared the reshoots. The finished film, released in 1962, was a huge success and was nominated for four Oscars, including one for Lancaster's performance.

Frankenheimer was next hired by producer John Houseman to direct All Fall Down, a family drama starring Eva Marie Saint and Warren Beatty. Due to production difficulties with Birdman of Alcatraz, All Fall Down was released first.

===The Manchurian Candidate===
Frankenheimer followed this with his best-regarded film, The Manchurian Candidate (1962). Frankenheimer and producer George Axelrod bought Richard Condon's 1959 novel after it had already been turned down by many Hollywood studios. After Frank Sinatra committed to the film, they secured backing from United Artists. The story of a Korean War veteran, brainwashed by the Communist Chinese to assassinate a candidate for President, co-starred Laurence Harvey, Janet Leigh, James Gregory, John McGiver, and Angela Lansbury.

Frankenheimer had to fight to cast Lansbury who had worked with him on All Fall Down and was only three years older than Harvey, who would play her son in the film. Sinatra's preference had initially been for Lucille Ball. The film was nominated for two Oscars, including one for Lansbury.

The film was unseen, either theatrically or on broadcast, for many years. Urban legend has it that the film was pulled from circulation due to the similarity of its plot to the death of President Kennedy the following year, but Frankenheimer states in the Champlin book that it was pulled because of a legal battle between the producer, Sinatra, and the studio over Sinatra's share of the profits. In any event, it was re-released to great acclaim in 1988.

===Seven Days in May===
Frankenheimer followed with another successful political thriller, Seven Days in May (1964). He again bought the rights to a bestselling book, this time by Charles Bailey II and Fletcher Knebel, and again produced the film with his star, this time Kirk Douglas. Douglas intended to play the role of the General who attempts to lead a coup against the President, who is about to sign a disarmament treaty with the Soviets. Douglas then decided he wanted to work with Burt Lancaster, with whom he had just costarred in another film. To entice Lancaster, Douglas agreed to let him play the General, while Douglas took the less showy lead role of the General's aide, who turns against him and helps the President.

The film, written by Rod Serling, also starred Fredric March as the President and Ava Gardner as a former flame of Lancaster's character. It was nominated for two Oscars.

===The Train===
The Train (1964) had been shooting in France for three days when star Lancaster had Arthur Penn, the original director, fired and called in Frankenheimer to save the film. As he recounts in the Champlin book, Frankenheimer used the production's desperation to his advantage in negotiations. He successfully demanded that his name be made part of the title, John Frankenheimer's The Train; that the uncredited French stand-by director, required by French tax laws, never be allowed to be on the film's set; that he be given total final cut on the film; and that he receive a Ferrari.

Again saddled with an unworkably long script, Frankenheimer threw it out and took the locations and actors left from the previous film and began filming, with writers working in Paris as the production shot in Normandy. The poorly chosen locations caused endless weather delays. The film contains multiple real train wrecks. The Allied bombing of a rail yard was accomplished with real dynamite, as the French rail authority needed to enlarge the track gauge. This can be observed by the shockwaves traveling through the ground during the action sequence. Producers realized after filming that the story needed another action scene, and reassembled some of the cast for a Spitfire attack scene that was inserted into the first third of the film. The screenplay was nominated for an Oscar.

===Seconds===
Seconds (1966) tells of an older man (John Randolph) given the body of a young man (Rock Hudson) through experimental surgery. It was poorly received on its release but has come to be one of the director's most respected and popular films subsequently. The film is an expressionistic, part-horror, part-thriller, part-science fiction film about the obsession with eternal youth and misplaced faith in the ability of medical science to achieve it.

The director of photography for Seconds was James Wong Howe, who is remembered for pioneering novel techniques in black-and-white cinematography. He was nominated for an Academy Award for his work on the film. Seconds was Frankenheimer and Howe's last film in black-and-white.

===Grand Prix===

Frankenheimer followed Seconds with his most spectacular production, 1966's Grand Prix. Shot on location at the Grand Prix races throughout Europe, using 65mm Cinerama cameras, the film starred James Garner and Eva Marie Saint. The making was a race itself, as John Sturges and Steve McQueen planned to make a similar movie titled Day of the Champion.

Due to their contract with the German Nürburgring, Frankenheimer had to turn over 27 reels shot there to Sturges. Frankenheimer was ahead in schedule anyway, and the McQueen/Sturges project was called off, while the German race track was only mentioned briefly in Grand Prix. Introducing methods of photographing high-speed auto racing that had never been seen before, mounting cameras on the cars, at full speed and putting the stars in the actual cars, instead of against rear-projections, the film was an international success and won three Oscars, for editing, sound, and sound effects.

===Late 1960s===
Frankenheimer's next film, 1967's all-star anti-war comedy The Extraordinary Seaman, starred David Niven, Faye Dunaway, Alan Alda and Mickey Rooney. The film was a failure at the box office and critically. Frankenheimer calls it in the Champlin book "the only movie I've made which I would say was a total disaster."

Following this the next year was The Fixer, about a Jew in Tsarist Russia and based on the novel by Bernard Malamud. The film was shot in Communist Hungary. It starred Alan Bates and was not a major success, but Bates was nominated for an Oscar.

Frankenheimer became a close friend of Senator Robert F. Kennedy during the making of The Manchurian Candidate in 1962. In 1968, Kennedy asked Frankenheimer to make some commercials for use in the presidential campaign, at which he hoped to become the Democratic candidate. On the night he was assassinated in June 1968, it was Frankenheimer who had driven Kennedy from Los Angeles Airport to the Ambassador Hotel for his acceptance speech.

The Gypsy Moths was a romantic drama about a troupe of barnstorming skydivers and their impact on a small midwestern town. The celebration of Americana starred Frankenheimer regular Lancaster, reuniting him with From Here to Eternity co-star Deborah Kerr, and it also featured Gene Hackman. The film failed to find an audience, but Frankenheimer claimed it was one of his favorites.

===1970s===
Frankenheimer followed this with I Walk the Line in 1970. The film, starring Gregory Peck and Tuesday Weld, about a Tennessee sheriff who falls in love with a moonshiner's daughter, was set to songs by Johnny Cash. Frankenheimer's next project took him to Afghanistan. The Horseman focused on the relationship between a father and son, played by Jack Palance and Omar Sharif. Sharif's character, an expert horseman, played the Afghan national sport of buzkashi.

Impossible Object, also known as Story of a Love Story, suffered distribution difficulties and was not widely released. Next came a four-hour film of O'Neill's The Iceman Cometh, in 1973, starring Lee Marvin, and the decidedly offbeat 99 and 44/100% Dead, a crime black comedy starring Richard Harris.

With his fluent French and knowledge of French culture, Frankenheimer was asked to direct French Connection II, set entirely in Marseille. With Hackman reprising his role as New York cop Popeye Doyle, the film was a success and got Frankenheimer his next job. Black Sunday, based on author Thomas Harris's only non-Hannibal Lecter novel, involves an Israeli Mossad agent (Robert Shaw) chasing a pro-Palestinian terrorist (Marthe Keller) and a PTSD-afflicted Vietnam vet (Bruce Dern), who plan a spectacular mass-murder involving the Goodyear blimp which flies over the Super Bowl. It was shot on location at the actual Super Bowl X in January 1976 in Miami, with the use of a real Goodyear Blimp. The film tested very highly, and Paramount and Frankenheimer had high expectations for it but it was not a hit.

Frankenheimer is quoted in Champlin's biography as saying that his alcohol problem caused him to do work that was below his own standards on Prophecy (1979), an ecological monster movie about a mutant grizzly bear terrorizing a forest in Maine.

===1980s===
In 1981, Frankenheimer travelled to Japan to shoot the cult martial-arts action film The Challenge, with Scott Glenn and Japanese actor Toshiro Mifune. He told Champlin that his drinking became so severe while shooting in Japan that he actually drank on set, which he had never done before, and as a result he entered rehab on returning to America. The film was released in 1982, along with his HBO television adaptation of the acclaimed play The Rainmaker.

In 1985, Frankenheimer directed an adaptation of the Robert Ludlum bestseller The Holcroft Covenant, starring Michael Caine. That was followed the next year with another adaptation, 52 Pick-Up, from the novel by Elmore Leonard. Dead Bang (1989) followed Don Johnson as he infiltrated a group of white supremacists. In 1990, he returned to the Cold War political thriller genre with The Fourth War with Roy Scheider (with whom Frankenheimer had worked previously on 52 Pick-Up) as a loose cannon Army colonel drawn into a dangerous personal war with a Soviet officer. It was not a commercial success.

===1990s===

Most of his 1980s films were less than successful, both critically and financially, but Frankenheimer was able to make a comeback in the 1990s by returning to his roots in television. He directed two films for HBO in 1994: Against the Wall and The Burning Season that won him several awards and renewed acclaim. The director also helmed two films for Turner Network Television, Andersonville (1996) and George Wallace (1997), that were highly praised.

Frankenheimer's 1996 film The Island of Doctor Moreau, which he took over after the firing of original director Richard Stanley, was the cause of countless stories of production woes and personality clashes and received scathing reviews. Frankenheimer was said to be unable to stand Val Kilmer, the young co-star of the film and whose disruption had reportedly led to the removal of Stanley half a week into production. When Kilmer's last scene was completed, Frankenheimer reportedly said, "Now get that bastard off my set." The veteran director also professed that "Will Rogers never met Val Kilmer". In an interview, Frankenheimer refused to discuss the film, saying only that he had a miserable time making it.

However, his next film, 1998's Ronin, starring Robert De Niro, was a return to form, featuring Frankenheimer's now trademark elaborate car chases woven into a labyrinthine espionage plot. Co-starring an international cast including Jean Reno and Jonathan Pryce, it was a critical and box-office success. As the 1990s drew to a close, he even had a rare acting role, appearing in a cameo as a U.S. general in The General's Daughter (1999). He earlier had an uncredited cameo as a TV director in his 1977 film Black Sunday.

===Last years and death===
Frankenheimer's last theatrical film, 2000's Reindeer Games, starring Ben Affleck, underperformed. But then came his final film, Path to War for HBO in 2002, which brought him back to his strengths – political machinations, 1960s America and character-based drama, and was nominated for numerous awards. A look back at the Vietnam War, it starred Michael Gambon as President Lyndon Johnson along with Alec Baldwin and Donald Sutherland. One of Frankenheimer's last projects was the 2001 BMW action short-film Ambush for the promotional series The Hire, starring Clive Owen.

Frankenheimer was scheduled to direct Exorcist: The Beginning, but it was announced before filming started that he was withdrawing, citing health concerns. Paul Schrader replaced him. About a month later he died suddenly in Los Angeles, California, from a stroke due to complications following spinal surgery at the age of 72.

==Archive==
The moving image collection of John Frankenheimer is held at the Academy Film Archive.

==Filmography==

=== Film ===

|-
! Year
! Title
! class="unsortable" | Notes
|-
| 1957
|The Young Stranger
|
|-
|1961
|The Young Savages
|
|-
| rowspan="3" |1962
|All Fall Down
|Nominated- Palme d'Or
|-
|Birdman of Alcatraz
|Nominated- DGA Award for Outstanding Directing – Feature Film
|-
|The Manchurian Candidate
|Also Producer<br>Nominated- Golden Globe Award for Best Director<br>Nominated- DGA Award for Outstanding Directing – Feature Film
|-
| rowspan="2" |1964
|Seven Days in May
|Nominated- Golden Globe Award for Best Director
|-
|The Train
|Replaced Arthur Penn
|-
| rowspan="2" |1966
|Seconds
|Nominated- Palme d'Or
|-
|Grand Prix
|Nominated- DGA Award for Outstanding Directing – Feature Film
|-
|1968
|The Fixer
|
|-
| rowspan="2" |1969
|The Extraordinary Seaman
|
|-
|The Gypsy Moths
|
|-
|1970
|I Walk the Line
|
|-
|1971
|The Horsemen
|
|-
| rowspan="2" |1973
|The Iceman Cometh
|
|-
|Impossible Object
|
|-
|1974
|99 and 44/100% Dead
|
|-
|1975
|French Connection II
|
|-
|1977
|Black Sunday
|
|-
|1979
|Prophecy
|
|-
|1982
|The Challenge
|
|-
|1985
|The Holcroft Covenant
|
|-
|1986
|52 Pick-Up
|
|-
|1989
|Dead Bang
|
|-
|1990
|The Fourth War
|
|-
|1991
|Year of the Gun
|Nominated- Deauville Critics Award for Best Feature Film
|-
|1996
|The Island of Dr. Moreau
|Replaced Richard Stanley
|-
|1998
|Ronin
|
|-
|2000
|Reindeer Games
|
|-
|2001
|Ambush
|Short film
|}

=== Television ===

|-
! Year
! Title
! class="unsortable" | Notes
|-
|1954
|You Are There
|Episode: "The Plot Against King Solomon"
|-
|1954-55
|Danger
|6 episodes
|-
|1955-56
|Climax!
|26 episodes
|-
|1956
|The Ninth Day
|Television film
|-
|1956-60
|Playhouse 90 
|27 episodes
|-
|1958
|Studio One in Hollywood 
|Episode: "The Last Summer"
|-
| rowspan="2" |1959
|DuPont Show of the Month
|Episode: "The Browning Vision"
|-
|Startime
|Episode: "The Turn of the Screw"
|-
|1959-60
|NBC Sunday Showcase
|2 episodes
|-
| rowspan="3" |1960
|Buick-Electra Playhouse
|3 episodes
|-
|The Snows of Kilimanjaro
| rowspan="2" |Television film
|-
|The Fifth Column
|-
| 1982
|The Rainmaker
|Television film
Nominated- CableACE Award for Best Direction in a Movie or Miniseries
|-
|1992
|Tales from the Crypt
|Episode: "Maniac at Large"
|-
| rowspan="2" |1994
|Against the Wall
|Television film
Primetime Emmy Award for Outstanding Directing for a Limited Series or Movie<br>Nominated- CableACE Award for Best Direction in a Movie or Miniseries<br>Nominated- DGA Award for Outstanding Directing – Miniseries or TV Film
|-
|The Burning Season
|Television film
Primetime Emmy Award for Outstanding Directing for a Limited Series or Movie<br>CableACE Award for Best Direction in a Movie or Miniseries<br>Nominated- Primetime Emmy Award for Outstanding Television Movie<br>Nominated- CableACE Award for Best Movie or Miniseries
|-
|1996
|Andersonville
|Television film
Primetime Emmy Award for Outstanding Directing for a Limited Series or Movie<br>Nominated- Primetime Emmy Award for Outstanding Television Movie<br>Nominated- DGA Award for Outstanding Directing – Miniseries or TV Film
|-
|1997
|George Wallace
|Television film
Primetime Emmy Award for Outstanding Directing for a Limited Series or Movie<br>CableACE Award for Best Miniseries<br>CableACE Award for Best Direction in a Movie or Miniseries<br>Nominated- Primetime Emmy Award for Outstanding Television Movie<br>Nominated- DGA Award for Outstanding Directing – Miniseries or TV Film
|-
|2002
|Path to War
|Television film
Nominated- Primetime Emmy Award for Outstanding Directing for a Limited Series or Movie<br>Nominated- Primetime Emmy Award for Outstanding Television Movie<br>Nominated- DGA Award for Outstanding Directing – Miniseries or TV Film
|}

==Awards and nominations==
British Academy Film Awards
* 1964 Train nominated for Best Film - Any Source
* 1962 Manchurian Candidate nominated for Best Film - Both Any Source and British

Cannes Film Festival
* 1966 Seconds nominated for Competing Film
* 1962 All Fall Down nominated for Competing Film

New York Film Critics Circle Award
* 1968 Fixer nominated for Best Direction
* 1968 Fixer nominated for Best Film

Venice Film Festival
* 1962 Birdman of Alcatraz nominated for Competing Film
* 1962 Birdman of Alcatraz won for San Giorgio Prize

Frankenheimer is also a member of the Television Hall of Fame, and was inducted in 2002.

==References==

{{reflist}}

==Further reading==
*Mitchell, Lisa, Thiede, Karl, and Champlin, Charles (1995). John Frankenheimer: A Conversation with Charles Champlin (Riverwood Press); {{ISBN|978-1-880756-09-6}}.
*Armstrong, Stephen B. (2008). Pictures About Extremes: The Films of John Frankenheimer (McFarland); {{ISBN|0-7864-3145-8}}.

==External links==
* {{IMDb name|1239}}
* [https://web.archive.org/web/20160303175738/http://opsroom.org/pages/intelligence/frankenheimer.html John Frankenheimer] OpsRoom.org
* [http://www.sensesofcinema.com/2006/great-directors/frankenheimer/ John Frankenheimer], Senses of Cinema, Issue 41 "Great Directors Series"
* {{EmmyTVLegends name|john-frankenheimer|John Frankenheimer}}
* [http://www.virtual-history.com/movie/person/1461/john-frankenheimer Literature on John Frankenheimer]
* [http://thehollywoodinterview.blogspot.com/2008/02/john-frankenheimer-hollywood-interview.html John Frankenheimer: The Hollywood Interview]

{{John Frankenheimer}}
{{Navboxes
| title = Awards for John Frankenheimer
| list =
{{EmmyAward MiniseriesDirector 1976-2000}}
{{The Life Career Award}}
{{2002 Television Hall of Fame}}
}}

{{Authority control}}

{{DEFAULTSORT:Frankenheimer, John}}
Category:1930 births
Category:2002 deaths
Category:American people of German-Jewish descent
Category:American people of Irish descent
Category:American television directors
Category:Television producers from New York City
Category:Film directors from New York City
Category:People from Queens, New York
Category:Williams College alumni
Category:Primetime Emmy Award winners
Category:Action film directors
Category:Catholics from New York (state)