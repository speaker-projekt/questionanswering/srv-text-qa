
{{short description|American computer scientist, open source developer, entrepreneur}}
{{Use dmy dates|date=June 2020}}
{{Infobox person
| name = 
| image = Ian_Murdock_interview_at_Holiday_Club_hotel_2008_(2).jpg
| caption = Murdock, in interview, April 2008
| image_size = 250px
| birth_name = Ian Ashley Murdock
| birth_date = {{Birth date|mf=yes|1973|04|28}}
| death_date = {{death date and age|mf=yes|2015|12|28|1973|4|28}}
| death_place = San Francisco, California, U.S.
| birth_place = Konstanz, West Germany
| death_cause = Asphyxiation due to suicide by hanging
| spouse = {{marriage|Debra Lynn||2008|reason=divorced}}
| children = 3
| father = Lawrence L. Murdock PhD
| occupation = Programmer
| alma_mater = Purdue University
| known_for = Debian
| height = 
| height_m = 1.78
}}

Ian Ashley Murdock (April{{nbsp}}28, 1973{{snd}} {{nbsp}}December 28, 2015) was an American software engineer, known for being the founder of the Debian project and Progeny Linux Systems, a commercial Linux company.

==Life and career==
Although Murdock's parents were both from Southern Indiana, he was born in Konstanz, West Germany, on April 28, 1973, where his father was pursuing postdoctoral
research. The family returned to the United States in 1975, and Murdock grew up in Lafayette, Indiana, beginning in 1977 when his father became a professor of entomology at Purdue University. Murdock graduated from Harrison High School in 1991, and then earned his bachelor's degree in computer science from Purdue in 1996.

While a college student, Murdock founded the Debian project in August 1993, and wrote the Debian Manifesto in January 1994. Murdock conceived Debian as a Linux distribution that embraced open design, contributions, and support from the free software community. He named Debian after his then-girlfriend (later wife) Debra Lynn, and himself. They later married, had three children, and divorced in January 2008.

In January 2006, Murdock was appointed Chief Technology Officer of the Free Standards Group and elected chair of the Linux Standard Base workgroup. He continued as CTO of the Linux Foundation when the group was formed from the merger of the Free Standards Group and Open Source Development Labs.

Murdock left the Linux Foundation to join Sun Microsystems in March 2007 to lead Project Indiana, which he described as "taking the lesson that Linux has brought to the operating system and providing that for Solaris", making a full OpenSolaris distribution with GNOME and userland tools from GNU plus a network-based package management system. From March 2007 to February 2010, he was Vice President of Emerging Platforms at Sun, until the company merged with Oracle and he resigned his position with the company.

From 2011 until 2015 Murdock was Vice President of Platform and Developer Community at Salesforce Marketing Cloud, based in Indianapolis.

From November 2015 until his death Murdock was working for Docker, Inc.

==Death==
Murdock died on December 28, 2015 in San Francisco. Though initially no cause of death was released, in July 2016 it was announced his death had been ruled a suicide. The police confirmed that the cause of death was due to asphyxiation caused by hanging himself with a vacuum cleaner electrical cord.

The last tweets from Murdock's Twitter account first announced that he would commit suicide, then said he would not. He reported having been accused of assault on a police officer after having been himself assaulted and sexually humiliated by the police, then declared an intent to devote his life to opposing police abuse. His Twitter account was taken down shortly afterwards.

The San Francisco police confirmed he was detained, saying he matched the description in a reported attempted break-in and that he appeared to be drunk. The police stated that he became violent and was ultimately taken to jail on suspicion of four misdemeanor counts. They added that he did not appear to be suicidal and was medically examined prior to release. Later, police returned on reports of a possible suicide. The city medical examiner's office confirmed Murdock was found dead.

==See also==
* List of Debian project leaders

==References==
{{Reflist|30em}}

==External links==
{{sisterlinks|d=Q92761|c=Category:Ian Murdock|b=no|v=no|voy=no|m=no|mw=no|species=no|q=no|n=no|wikt=no|s=no}}
* {{Official website}} ([https://web.archive.org/web/20151229024342/https://ianmurdock.com/ archived])
* {{citation |url = https://www.linuxjournal.com/article/2841 |title = Overview of the Debian GNU/Linux System| publisher=LinuxJournal | date={{date|1994-10-01|mdy}}|first=Ian|last=Murdock }}.
* {{citation |url = http://tlltsarchive.org/archives/tllts_83-05-11-05.mp3 |publisher = The Linux Link Tech Show |title = Interview (starts 22:20) |format = MP3 |date = 11 May 2005 |website = tlltsarchive.org }}
* {{citation |url = http://www.jeffratliff.org/mirror/TLLTS/mp3/tllts_134-lwe-04-07-06.mp3 |publisher = The Linux Link Tech Show |title = Interview (starts 48:04) |format = MP3 |date = 4 July 2006 |website = jeffratliff.org }}
* https://archive.org/details/IanMurdockHomepage.tar
* https://archive.org/download/AutopsyIanMurdockDebianLinuxFounder/Autopsy-Ian-Murdock-Debian-Linux-Founder.pdf

{{s-start}}
{{s-new|creation|reason=Founding of Debian Project}}
{{s-ttl|title=Debian Project Leader|years=August 1993 – March 1996}}
{{s-aft|after=Bruce Perens}}
{{s-end}}

{{Debian}}
{{Linux}}
{{Linux people}}
{{Authority control}}

{{DEFAULTSORT:Murdock, Ian}}
Category:1973 births
Category:2015 deaths
Category:American computer programmers
Category:Debian Project leaders
Category:Free software programmers
Category:Open source people
Category:People from Fort Wayne, Indiana
Category:People from Konstanz
Category:People from Indianapolis
Category:Suicides by hanging in California
Category:Purdue University alumni
Category:Sun Microsystems people