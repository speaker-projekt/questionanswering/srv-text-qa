;John Adams
;Thomas Jefferson

;George Washington
;John Adams

;1792;1808
;Democratic-Republican;1809;1828
;National;Republican;1828;1830
;1830;1834
;Whig;1834;1848

;John Adams
;Abigail Smith

;Adams


;Harvard University (AB
;Leiden University


;John Quincy Adams;k;Quincy;k;Quincy;Massachusetts;Braintree;Adams;k;Adams;July 11, 1767;February 23, 1848;American;diplomat;lawyer;sixth;president;United States;1825;to;1829;previously;eighth;United States;Secretary of State;1817;to;1825;He;Adams;ambassador;United States;Senate;House of Representatives;Massachusetts;his;John Adams;second;U.S.;president;1797;to;1801;and First;Abigail Adams;He;Democratic-Republican Party;the mid-1830s;Whig Party;his;he
;now;Quincy;Massachusetts;Braintree;Adams;Europe;diplomat;his;his;United States;Adams;Boston;1794;President;George Washington;Adams;U.S.;ambassador;Netherlands;Adams;1801;Thomas Jefferson;president;Massachusetts;Adams;United States;Senate;1802;Adams;Federalist Party;1809;Adams;U.S.;ambassador;Russia;President;James Madison;Democratic-Republican Party;Adams;Madison;American;War of;1812;he;1817;president;James Monroe;Adams;Secretary of State;his;Adams;American;Florida;Monroe Doctrine;U.S.;He
;1824;Adams;Andrew Jackson;William H. Crawford;Henry Clay;Democratic-Republican Party;candidate;House of Representatives;president;Adams;Clay;president;Adams;Latin;America;Congress;his;Adams;Democratic-Republican Party;two;one;National Republican Party;President;Adams;Democratic Party;Andrew Jackson;Democrats;Adams;National Republican;Jackson;Adams;1828;second;president;his;his
;Adams;House of Representatives;1831;1848;he;his;John Tyler;Confederate;representative;He;Governor;Massachusetts;Senate;Adams;the early 1830s before;Whig Party;President;Jackson;Congress;Adams;Southern;Democratic Party;his;he;Texas;Mexican;American;War;war;grip;Congress;He;he;House of Representatives;He;Adams;one;American;president;Congress;him;he
;John Quincy Adams;July 11, 1767;John;Abigail Adams;Smith;Braintree;Massachusetts;now;Quincy;Colonel;John Quincy;Quincy;Massachusetts;He;his;Adams;James Thaxter;clerk;Nathan Rice;his;his;October 2018;1779;1848;He;his;he;he;he;ten;Adams;Braintree;his;American;Revolution;John Adams;his;his;him;Adams;Virgil;Horace;Plutarch;Aristotle;his
;1778;Adams;Europe;John Adams;American;France;Netherlands;his;Adams;French;Greek;Leiden University;1781;Adams;Saint Petersburg;Russia;secretary of American diplomat;Francis Dana;he;Netherlands;1783;Great Britain;1784;He;his;Adams;Europe;United States;he;his;he;his
;Adams;United States;1785;Harvard College;the following year;second;1787;He;his;Harvard;Theophilus Parsons;Newburyport;Massachusetts;1787;to;1789;he;Adams;United States;1789;first;Vice President;United States;he;his;1790;Adams;Boston;his;attorney;he;his
;Adams;his;1791;Britain;model;France;he;Two years later;French;diplomat;President;George Washington;French;Revolutionary;he;1794;Washington;Adams;U.S.;ambassador;Netherlands;Adams;his;Adams;United States;French;Revolutionary;ambassador;Netherlands;U.S.;His;Netherlands;John Jay;Great Britain;his;he;Adams;United States;Alexander Hamilton;Democratic-Republican Party;Thomas Jefferson
;Adams;the winter of 1795;1796;London;Louisa Catherine Johnson;second;American;merchant;Joshua Johnson;he;April 1796;Louisa;Adams;Adams;England;his;he;his;he;his;Adams;Louisa;United States;July 1797;his;he;Adams;president;1825;Louisa;first First;United States;2017;Melania Trump;second;First;Lady;United States;Joshua Johnson;England;Adams;Johnson;Louisa;his;him;Adams;Louisa;his;he;his
;1796;Washington;Adams;U.S.;ambassador;Portugal;year;John Adams;Jefferson;1796;Adams;president;U.S.;ambassador;Prussia;he;his;Adams;Prussian;Berlin;Thomas Boylston Adams;his;his;his;State Department;Adams;Prussia;Sweden;President;Adams;Europe;his;him;1799;Adams;United States;Prussia;Sweden;he;United States;1801;Prussian;Silesia;Silesia;He;his;1800;Jefferson;John Adams;Adams;early 1801;his
;United States;Adams;Boston;April 1802;Massachusetts Senate;his;he;November;year;United States;House of Representatives;he;February 1803;Massachusetts;Adams;United States;Senate;Adams;Congress;Samuel Chase;Federalist Party;his;he
;Adams;Jefferson;1800;Federalist Party;he;Adams;Timothy Pickering;Federalist Party;Adams;Britain;His;England;Adams;Jefferson;Louisiana;Purchase;Adams;Congress;1806;Britain;attacks;American;Adams;Federalists;British;Jefferson;he;Adams;Embargo Act;1807;Massachusetts;Adams;several months;Adams;Senate;his
;Senate;Adams;professor;Brown University;Professor;Harvard University;Adams;democracy;Jacksonian Era;his;he;Adams;Ciceronian;his;republican;British;philosopher;David Hume;He;Adams;republican;American;Demosthenic;1810;His;Britain;United States;the second decade of the 19th century
;Senate;Adams;Massachusetts;Democratic-Republican;he;1809;Supreme Court;United States;Fletcher;Peck;Supreme Court;Adams;Georgia;he;year;President;James Madison;Adams;first;United States;Minister;Russia;1809;Adams;recently;Federalist Party;Jefferson;Madison Administration;his;him;Adams;Europe;Russia;his
;Baltic Sea;Adams;Russian;St.;Petersburg;October 1809;Russian;Nikolay Rumyantsev;Tsar Alexander;Russia;He;Adams;American;France;Britain;Napoleonic;War;Louisa;Russia;Russian;she;Adams;French;Emperor;Napoleon;Russia;French;his;February 1811;Adams;President;Madison;Associate Justice of the United States Supreme Court;Senate;Adams;Joseph Story
;Adams;United States;war;Britain;early 1812;war;British;attacks;American;British;he;United States;war;Britain;War;1812;Tsar Alexander;Britain;United States;President;Madison;Adams;Secretary of the Treasury;Albert Gallatin;Senator;James A. Bayard;war;Gallatin;Bayard;St.;Petersburg;July 1813;British;Tsar Alexander;Adams;Russia;April 1814;Ghent;Adams;Gallatin;Bayard;two;American;Jonathan Russell;Speaker;Henry Clay;Adams;Gallatin;Bayard;Russell;Clay;he
;British;United States;Indian;American;Great Lakes;American;American;Plattsburgh;November 1814;Liverpool;U.S.;Adams;British;his;December 24, 1814;United States;war;Adams;Paris;the Hundred Days;Napoleon;he
;May 1815;Adams;President;Madison;U.S.;ambassador;Britain;him;Clay;Gallatin;Adams;Britain;Adams;ambassador;American;war;president;James Monroe;Secretary of State;Adams;he;several years;Europe;Adams;United States;August 1817
;Adams;Secretary of State;Monroe;eight-year;1817;to;1825;secretary;1818;Great Britain;Spain;his;Adams;American;independent;Latin;America;war;Spain;European;Andrew Jackson;Florida;Henry Clay;Congress;Spain;Adams;Biographer;James Lewis;Spanish;He;he;he;Monroe;last four years;Secretary of State;war;White House;his;he;his;his
;War;1812;Adams;war;European;Britain;he;War;1812;He;Adams;Hartford;Convention;anti-war;Federalists;Madison;One;Adams;Latin;America;Spain;Peninsular;War;Adams;1820;his
;Monroe;Adams;Latin;American;Great Britain;French;North;American;Spanish;Empire;president;secretary of state;Adams;Monroe;Monroe;his;he;Monroe;five-person;Adams;Secretary of the Treasury;William H. Crawford;Secretary of War;John C. Calhoun;Secretary of the Navy;Benjamin Crowninshield;Attorney General;William Wirt;his;Adams;Calhoun;Crawford;Monroe;1824
;ambassador;Britain;Adams;War;1812;his;1817;two;Rush–Bagot Treaty;Great Lakes;two;1818;Canada;United States;Great Lakes;49th;Rocky Mountains;Oregon;United Kingdom;United States;United States;British;American
;Adams;Spanish;United States;South;Spain;Florida;U.S.;Spain;Indian;Florida;United States;West;New;Spain;United States;Louisiana;Purchase;United States;Spanish;Adams;Luis de Onís;Spanish;minister;United States;Florida;United States;New;Spain;Seminole;War;December 1818;Monroe;General;Andrew Jackson;Florida;Seminoles;Georgia;Jackson;Spanish;St. Marks;Pensacola;two;Englishmen;his;Jackson;Adams;Monroe;Jackson;he;Adams;Spain;Jackson;Spain;Spain;United States;he;British;United States;Jackson;execution;two;British
;Spain;United States;Spain;Florida;United States;American;Rio Grande;Spain;Mexico;American;Sabine;River;Monroe;Adams;Sabine;River;Spain;Oregon;Country;he;Adams;American;Oregon;Country;Asia;he;Spanish;Pacific Northwest;Monroe;Florida;Southerners;Spain;United States;Adams–Onís Treaty;February 1821;Adams;he;1824;Monroe;United States;Oregon;1824;Russian;Alaska;54;40
;Spanish;Empire;Monroe;second;Adams;Monroe;Prussia;Austria;Russia;Spain;1822;Monroe;Latin;American;Argentina;Mexico;1823;British;Foreign Secretary;George Canning;United States;Britain;Adams;Adams;Monroe;United States;European;Americas;United States;European;December 1823;annual;Congress;Monroe;Adams;his;Monroe Doctrine;United States;first;Europe;Americas;Britain;France;Native Americans;one;U.S.
;Secretary of State;Adams;one;Monroe;three;1824;Henry Clay;John C. Calhoun;William H. Crawford;Adams;Monroe;Crawford;Clay;Calhoun;Adams;the;Second Bank;United States;Federalist Party;War;1812;Democratic-Republican Party;Adams;president;his;his;him;Adams;he;his
;Adams;vice president;General;Andrew Jackson;Adams;Jackson;one;one;he;1824;Jackson;president;Jackson;New Orleans;his;Democratic-Republican;1824;Adams;New;England;candidate;Adams;New;England;Clay;Jackson;West;Jackson;Crawford;South
;3;1825
;Republican;Adams
;Jacksonian;Jackson
;Independent;Crawford
;Connecticut
;Illinois
;Kentucky
;Louisiana
;Maine
;Maryland
;Massachusetts
;Missouri
;New Hampshire
;New York
;Ohio
;Rhode Island
;Vermont
;Alabama
;Indiana
;Mississippi
;New Jersey
;Pennsylvania
;South Carolina
;Tennessee
;Delaware
;Georgia
;North Carolina
;Virginia
;1824;Jackson;Electoral College;99;261;Adams;84;Crawford;41;Clay;37;Calhoun;vice president;Adams;New;England;New York;six;he;Jackson;New Jersey;Pennsylvania;Northwest;he;candidate;House;Twelfth;House;three;one;three;Clay;House;his
;Adams;Clay;House of Representatives;his
;Adams;March 4, 1825;He;his;he;he;He;Adams;General Welfare Clause;Congress;He;he
;Adams;weekly;he;Monroe;Adams;Monroe;he;his;Samuel L. Southard;New Jersey;Secretary of the Navy;William Wirt;Attorney General;John McLean;Ohio;General;his;Adams;first;Secretary of War;Secretary of the Treasury;Andrew Jackson;William Crawford;Adams;James Barbour;Virginia;Crawford;War;Treasury Department;Richard Rush;Pennsylvania;Adams;Henry Clay;Secretary of State;Clay;1824;his;Clay;Corrupt Bargain;Clay;West;him
;1825;annual;Congress;Adams;his;He;Adams;Washington;D.C.;New Orleans;Interior;He;Adams;Western;Adams;American;System
;Adams;his;South;Adams;abolitionist;the states;he;president;Congress;Adams;Senate;House;His;Adams
;Adams;his;1824;1828;United States Army Corps of Engineers;river;Adams;National Road;National Road;Cumberland;Maryland;Zanesville;Ohio;he;Adams;Chesapeake;Ohio;Canal;Chesapeake & Delaware Canal;Louisville;Portland;Canal;Falls;Ohio;Ohio;River;Ohio;Indiana;Dismal Swamp Canal;North Carolina;first;United States;Baltimore;Ohio Railroad;Adams
;1825;Jackson;Adams;Adams;Clay;Jackson;flood;him;1825;Jackson;Tennessee;1828;Adams;Monroe;Vice President;Calhoun;president;Clay;Clay;Adams;he;Adams;December 1825;annual;Congress;Francis Preston Blair;Kentucky;Thomas Hart Benton;Missouri;Adams;first;19th;United States Congress;anti-Adams;Benton;Hugh Lawson White;Crawfordites;Martin Van Buren;Nathaniel Macon;Robert Y. Hayne;George McDuffie;Clay;Adams;North;Edward Everett;John Taylor;Daniel Webster;Congress;his;Adams;National Republicans;Jackson;Democrats;Adams Men
;1826;Adams;Adams;Speaker;John Taylor;Andrew Stevenson;Jackson;Adams;United States;Congress;president;Van Buren;Calhoun;Jackson;1828;Van Buren;Crawford;Jackson;Adams;Adams;his;Adams;non-partisan;he;his
;first;Adams;South;New;England;his;he;his;Jacksonians;1827;Western;New;England;Van Buren;Congress;Adams;he;his;Adams;1828;Tariff of Abominations;Adams;South;he
;Adams;Native Americans;the 1820s;Adams;United States;Native Americans;Adams;Indian;Springs;Governor;Georgia;George Troup;Muscogee;his;Adams;Muscogee;January 1826;Muscogee;Georgia;Troup;Georgian;Muscogee;Georgia;Muscogee;third;Troup;Native Americans;Indian;his
;One;Adams;American;Denmark;Hanseatic League;Scandinavian;Prussia;Federal Republic of Central;America;His;Hawaii;Tahiti;Denmark;Sweden;American;Adams;British;West Indies;United States;Britain;1815;British;Western Hemisphere;United States;British;American;West Indies;1823;United States;Britain;1825;Britain;United States;British;West Indies;Adams;Adams;British;two;British;West Indies;Adams;United States
;Texas;Mexico;President;Adams;Latin;America;North;America;Adams;Clay;Latin;America;British;United States;Congress;Panama;1826;Simón Bolívar;Clay;Adams;independent;Americas;Adams;Van Buren;Van Buren;Panama Congress;President;Washington;Southerners;Haiti;United States;Senate;Congress;Panama;Senate
;Jackson;Adams;Jackson;executive;Adams;he;early 1827;Jackson;Rachel;first;his;her;Jackson;Adams;Jacksonian;Adams;pro-Adams;Jackson;past;duels;him;Adams;Clay;American;System;Jackson;Adams
;Vice President;Calhoun;Jackson;Adams;Secretary of the Treasury;Richard Rush;his;1828;first;United States;two;two;Southerners;Jackson;178;261;56%;Jackson;50.3%;72.6%;future;candidate;Jackson;Theodore Roosevelt;1904;Adams;second;one-term;president;him;his;1828;two;president;1828;1824;Adams;Jackson;one;four;him
;Adams;1828;suicide;George Washington Adams;1829;his;he;his;Jackson;Treasury;Auditor;Tobias Watkins;embezzlement;He;his;once;Adams;Jackson;the decades;1828;Adams;United States;House of Representatives;1830;his;his;he;His;his;nine;1831;1848;he;his;Adams;Andrew Johnson;Congress;Adams;Anti-Masonic Party;National Republican Party;Massachusetts;Federalists;Adams;his;Anti-Masonic Party;first;third;general;anti-elitism
;Adams;Washington;64 years old;Speaker;Andrew Stevenson;Adams;chairman;Committee on Commerce;he;Anti-Masonic Party;Congress;Jackson;Jackson;Adams;he;Stevenson;Jackson;Adams;Jacksonian;Adams;chairman;Adams;Nullification;Southern;1828;South Carolina;Adams;1832;South Carolina;Clay;Calhoun;1833;Adams;Southern;he;Adams;Southerners;Jackson;Democratic Party
;Anti-Masonic Party;Adams;1833;Massachusetts;four-way;Adams;National;Republican;candidate;Democratic;candidate;candidate;Working Men's Party;National;Republican;candidate;John Davis;40%;Adams;second;29%;candidate;Adams;Davis;his;Adams;Senate;1835;Anti-Masons;National Republicans;Jackson;National Republican;his;his;1835;Adams;House of Representatives;his
;the mid-1830s;Anti-Masonic Party;National Republicans;Jackson;Whig Party;1836;Democrats;Martin Van Buren;Whigs;president;Adams;Van Buren;he;Adams;Whig Party;Congress;Adams;President;Van Buren
;Republic of;Texas;Mexico;Texas;1835;1836;Texas;Americans;Southern;United States;1829;Mexican;United States;Texas;Texas;Adams;Texas;one;he;Adams;Texas;secretary of state;Mexico;Texas;he;he;Texas;Southern;Cuba;He;Adams;Van Buren;Texas;his
;Whig;William Henry Harrison;Van Buren;1840;Whigs;Congress;first;Harrison;Adams;Whig;Democratic;his;Harrison;April 1841;Vice President;John Tyler;Southerner;Adams;Henry Clay;Whigs;American;System;Adams;Tyler;Virginia;Jeffersonian;Tyler;Whig;Tyler;Adams;chairman;Tyler;Adams;Tyler;his;Whigs;Senate;Tyler
;Tyler;Texas;his;1844;Adams;Senate;He;Texas;1844;Southerners;Van Buren;1844;Democratic;National Convention;James K. Polk;Andrew Jackson;once;Adams;Polk;Henry Clay;1844;he;his;Liberty Party;abolitionist;third;Clay;New York;He;Tyler;end in March 1845;once;Congress;Tyler;Senate;two-thirds;Senate;1844;Tyler;Congress;Congress;Adams;Texas;United States;war;Adams;Congress;Democrats;Whigs;Texas;United States;1845
;Adams;James K. Polk;House of Representatives;Adams;president;Southern Democrat;him;Adams;Oregon;Country;United States;Britain;President;Polk;Oregon Treaty;two;49th;Polk;Mexican;Alta California;Mexico;he;Mexican;California;American;Texas;General;Zachary Taylor;Rio Grande;Southern;Texas;United States;his;Taylor;Mexican;Rio Grande;Polk;war;early 1846;Mexico;American;Whigs;Mexico;war;Congress;war;House;174-to-14;Adams;Polk;one;14;war;Mexico;he;1846;Adams;Mexican;American;War;1848;he;his
;the 1830s;United States;Adams;Congress;leader;his;he;one;about a day;war;Earth;his;he;he;1820;He;his
;1836;Adams;District of Columbia;House of Representatives;Democrats;Southern Whigs;Adams;late 1836;Adams;present;Southern;He;his;him;Adams;another seven years;1844
;1841;Lewis Tappan;Ellis Gray Loring;Adams;United States;Adams;Supreme Court;African;Spanish;Adams;February 24, 1841;four hours;Africans;His
;Adams;1829;British;scientist;James Smithson;he;his;Smithson;Henry James Hungerford;Smithson;United States;he;his;1835;President;Andrew Jackson;Congress;US$500,000;US$75 million;2008;dollars;Adams;United States;his;Adams;Congress;the future;Smithsonian Institution
;Congress;Adams;Congress;Adams;Congress;Congress;Adams;Congress;United States;July 1, 1836;Adams;Congress;Smithsonian Institution;1846
;mid-November;1846;78-year-old;president;stroke;him;a few months;Congress;he;his;Adams;House;February 13, 1847
;February 21, 1848;House of Representatives;United States Army;Mexican;American;War;Adams;critic;war;he;Speaker;Robert Charles Winthrop;He;Adams;cerebral hemorrhage;Two days later;February 23;7:20 p.m.;Speaker;Capitol Building;Washington;D.C.;Charles Francis;he;his;his;his;his;Earth;His;present;Abraham Lincoln;representative;Illinois;his
;Congressional Cemetery;Washington;D.C.;Quincy;Massachusetts;First Parish Church;Hancock Cemetery;His;he;Louisa;1852;United First Parish Church;John;Abigail;his;his;Adams;Hancock Cemetery;J.Q. Adams
;Adams;Louisa;three;Louisa;1811;1812;first;George Washington Adams;1801;1829;first;president;Adams;her;his;George;second;John;1803;1834;George;alcoholism;1829;he;John;illness;1834;his;Adams;Charles Francis Adams Sr.;leader;Whigs;Northern;Whig Party;Charles;Free Soil Party;candidate;1848;Republican Party
;Adams;his;He;Historian;Paul Nagel;Abraham Lincoln;Adams;early years;him;he;Adams;his;him;his;his;he;his;his;He;his;he;his;he;his;American;Revolution;Adams;his;his;his;he;his;Adams;alcoholism;his;her;him;her;her;her;biographer;Nagel;Louisa Johnson;Johnson;1797;Adams;Johnson;His;his;him;his
;Adams;first;president;his;he;John Quincy Adams;U.S.;president;Dean Simonton;professor;UC Davis;165;his
;Adams;one;American;president;him;Adams;1824;his;Adams;politician;the Second;revolutionary;He;Biographer;historian;William J. Cooper;Adams;American;the late 20th century;he;his;Cooper;Adams;first;United States;Historian;Daniel Walker Howe;Adams;his;Adams;day;his;Russell Kirk;Adams;conservative;stylist;James Parker;one;three;White House;Abraham Lincoln;Barack Obama;He;him
;John Quincy Adams Birthplace;now;Adams National Historical Park;Adams House;one;twelve;Harvard University;John Adams;John Quincy Adams;Adams;Harvard;1870;Charles Francis;first;United States;his;14,000;twelve;Peacefield;Adams National Historical Park;Quincy;Massachusetts
;Adams;Quincy;United States;Quincy;Illinois;Adams County;Illinois;Adams County;Indiana;Adams;Adams County;Iowa;Adams County;Wisconsin;John Adams;John Quincy Adams
;1843;Adams;United States;president;William Henry Harrison;1841;his;Smithsonian Institution
;Adams;PBS;1976;David Birney;William Daniels;Marcel Trenchard;Steven Grover;Mark Winkworth;he;Anthony Hopkins;1997;Ebon Moss-Bachrach;Steven Hinkle;2008;HBO;John Adams;HBO;He
;Adams

;United States
;United States
;United States;Congress;1790;1899

;Quincy Institute for Responsible Statecraft
;Lewis Jr;James E. John Quincy Adams;Rowman & Littlefield Publishers;2001
;Edward;John Quincy Adams;Henry Graff;3rd;2002;http://www.presidentprofiles.com/Washington-Johnson/Adams-John-Quincy.html
;Waldstreicher;David;John Quincy Adams;1779;1821;America;20170;727
;https://www.whitehouse.gov/1600/presidents/johnquincyadams;White House
;http://www.masshist.org/jqadiaries/;John Quincy Adams;Massachusetts Historical Society
;http://www.c-span.org/video/?122555-1/life-portrait-john-quincy-adams;John Quincy Adams;C-SPAN;American;April 18, 1999
