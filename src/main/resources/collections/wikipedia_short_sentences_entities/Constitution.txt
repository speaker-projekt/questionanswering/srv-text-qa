
;United Kingdom

;India;146,385;English-language;Monaco;3,814;San Marino;1600;United States;more than 19 years
;French;Pope;now;apostolic
;William Blackstone;revolutionary;Blackstone;American;he
;Scott Gordon
;charter;charter

;Hegel;centuries
;1789;United States of America;U.S.;220;independent
;the late 18th century;Thomas Jefferson;20 years;19 years;more than 10 years;10%;last more than 1 year;French;1791
;2009;16 months;Myanmar;2008;more than 17 years;Japan;1946;no more than a week;Japan;Romania;1938;less than a month;democratic;Non-democratic;North Korea
;Iraq;Ernest de Sarzec;1877;Sumerian;king;Urukagina;Lagash;2300 BC;his
;2050 BC;Lipit-Ishtar of Isin;Hammurabi;Babylonia;Assyrian
;621 BC;scribe;Draco;Athens;594 BC;Solon;Athens;Cleisthenes;Athenian;democratic;508 BC
;Aristotle;350 BC;first;constitutionalism;general;he;Athens;Politics;Nicomachean Ethics;day;Athens;Sparta;Carthage;his;he;his;democratic;He;he;he;He
;first;450 BC;Twelve;Roman;438 AD;Eastern Empire;534;Europe;Leo III;Isaurian;740;Basilica of Basil;878
;Ashoka;the 3rd century BC;king;India
;Germanic;Western Roman Empire;One;first;Germanic;471 AD;Lex Burgundionum;Germans;Romans;Franks;500;506;Breviarum;Alaric II;king;Visigoths;Codex Theodosianus;Roman;Edictum Rothari;643;Lex Visigothorum;654;Lex Alamannorum;730;Lex Frisionum;785;Anglo-Saxon;England;602;893;Alfred;two;Saxon;Christian;Doom;England
;Japan;Seventeen-article;604;Prince Shōtoku;Asian;Buddhist
;Medina;Ṣaḥīfat al-Madīna;Charter;Medina;Islamic;prophet;Muhammad;Yathrib;leader;his;he;Muhammad;Yathrib;Medina;Muslims;Jews
;Reuven Firestone;Jihād;war;Islam;1999;118
;Muhammad;Islam Online;Khazraj;Medina;Muslim;Jewish;pagan;Medina;fold;one;Medina;622
;Wales;Cyfraith Hywel;Hywel Dda;942;950
;Yaroslav the Wise;Prince;1017;1054;Ruska Pravda;Rus;the 15th century
;England;Henry;Charter;1100;king;first;his;English;King;John;Magna Carta;1215;Magna Carta;king;first;39;Magna Carta
;English;king;House of Commons
;Saint;first;Serbian;1219;St.;Sava;Nomocanon;Roman Law;Canon;Serbian;Serbian;Saint Sava;Serbian Nomocanon;1208;Mount Athos;Fourteen;Titles;Stefan the Efesian;Nomocanon;John Scholasticus;Ecumenical Council;Aristinos;Joannes Zonaras;Prohiron;Byzantine;he;Nomocanon;Byzantine;St. Sava;Serbia;Prohiron;Serbian;medieval;Zakonopravilo
;Stefan Dušan;Emperor;Serbs;Greeks;Dušan;Dušanov Zakonik;Serbia;two;1349;Skopje;1354;Serres;second;Serbian;St. Sava;Nomocanon;Zakonopravilo;Code;171;172;Dušan's Code;Basilika;VII;1;16;17
;1222;Hungarian;King;Andrew II;Golden;Bull;1222
;1220;1230;Saxon;administrator;Eike von Repgow;Germany;1900
;1240;Coptic;Egyptian;Christian;writer;Abul Fada'il Ibn al-'Assal;Arabic;Ibn al-Assal;apostolic;Byzantine;his;Ethiopia;1450;Zara Yaqob;first;Sarsa Dengel;beginning in 1563;Ethiopia;1931;first;Emperor;Haile Selassie I.
;Catalonia;Catalan;1283;two centuries;Usatges;Barcelona;1716;Philip V;Spain;Nueva Planta;Catalonia;Catalan Courts;medieval;king
;Kouroukan Founga;13th century;charter;Mali;Empire;1988;Siriman Kouyaté
;1356;Nuremberg;Emperor;Charles IV;more than four hundred years
;China;Hongwu Emperor;first;1375;1398;he;his;the next 250 years
;today;San Marino;Sancti Marini;six;first;62;executive;1600;1300;Codex Justinianus;today
;1392;Carta;de Logu;Arborea;Eleanor;Sardinia;Charles Felix;April 1827;Carta;Sardinian
;Gayanashagowa;Great Law of Peace;1190 AD;recently;1451;Sachems;Iroquois League;Sachem
;1634;Kingdom of Sweden;1634;Chancellor;Sweden;Axel Oxenstierna;king;Gustavus Adolphus;first
;1639;Colony of Connecticut;first;North;American;Connecticut;Connecticut
;English;Protectorate;Oliver Cromwell;English;Civil;War;first;1653;to;1657;Parliament
;Major-General;John Lambert;1653;Army Council;1647;King;Charles;First;English;Civil;War;Charles;the start of the Second;Civil;War;New Model Army;Putney Debates
;January 4, 1649;Rump Parliament;England
;Parliament;December 15, 1653;Oliver Cromwell;the following day;21;executive;at least five months
;May 1657;England;second;Christopher Packe;Oliver Cromwell;Parliament;independent;king;Triennial;Parliament;25 May;Cromwell
;European;Corsican;1755;Swedish;1772
;British;North;America;13;United States;1776;1777;American;Revolution;United States;Massachusetts;Connecticut;Rhode Island;Massachusetts;1780;U.S.;Connecticut;Rhode Island;first;1818;1843
;model;Age of Enlightenment;Thomas Hobbes;Jean-Jacques Rousseau;John Locke;model;democracy
;Host;1710;Pylyp Orlyk;Host;Zaporozhian-Ukrainian Republic;Charles XII;Sweden;democratic;executive;Montesquieu;executive;General Council;Orlyk;independent;Ukrainian State;his
;Corsican Constitutions;1755;1794;Jean-Jacques Rousseau
;Swedish;1772;King;Gustavus III;Montesquieu;king;torture;Voltaire
;United States;June 21, 1788;Polybius;Locke;Montesquieu;republicanism
;Polish;Lithuanian;May 3, 1791;Enlightenment;Poland;King;Stanislaw August Poniatowski;Stanisław Staszic;Scipione Piattoli;Julian Ursyn Niemcewicz;Ignacy Potocki;Hugo Kołłątaj;first;Europe;second;one;American
;French;1791
;1811;Venezuela;first;Venezuela;Latin;America;Cristóbal Mendoza;Juan Germán Roscio;Caracas;one year later
;March 19;Spanish;1812;Cadiz;Spanish;French;Spanish;model;liberal;South;European;Latin;American;Portuguese;1822;Italian;Kingdom;Two;Norwegian;1814;Mexican;1824
;Brazil;1824;Brazilian;leader;Portuguese;prince;Pedro;king;Portugal;Pedro;1822;first;emperor;Brazil;1889;Republican;model
;Denmark;Napoleonic Wars;Norway;Sweden;Sweden;1809;Riksdag;king;Norwegians;democratic;liberal;1814;American;revolutionary;French;Spanish;one
;first;Swiss;September 1848;1878;1891;1949;1971;1982;1999
;Serbian;1811;Serbia;few decades later;1835;first;Serbian;Kragujevac;February 15, 1835
;Canada;July 1, 1867;British;America;British Parliament;a century later;Canadian;Parliament;Canadian;Charter;1867;1982;Canada
;first;Plato;Aristotle;Cicero;Plutarch
;Roman;war
;England;Civil;War;Thomas Hobbes;Samuel Rutherford;John Milton;James Harrington;Robert Filmer;one;Henry Neville;James Tyrrell;Algernon Sidney;John Locke;first
;Montesquieu;executive
;Orestes Brownson;Brownson;three;first;second;third;The second;Brownson;Brownson
;accident;Brownson


;executive;executive;executive

;one;one


;two

;democracy;Estonia
;centuries;Westminster System;Britain;Parliament;general;Thursdays;British
;Australia;Commonwealth of Australia;Commonwealth;Statute of Westminster Adoption Act;1942;Australia;1986;Australia;July 2020;Australia;July 2020
;Canada;British North America Acts;1867;Canada;1982;British;Canadian;Canadian;52;2;Constitution Act;1982;52;2;Canada;1763;Canada;Canada
;Israel;Parliament;United Kingdom;U.K.;2011;general;five years





;one
;Czech Republic;Germany;Turkey;Greece;Italy;Morocco;Islamic;Republic of;Iran;Brazil;Norway;India;Colombia

























;one



;model;Baron;Montesquieu;three;executive
;president;president
;Parliament;prime minister;United Kingdom;prime minister;prime minister;general
;independent;anti-corruption
;three
;UK;Northern;Ireland;Scotland;Wales;England;Spain
;Canada;United States

;European Union;Union-wide;previously

;Italian;Giovanni Sartori;independent;Soviet;Union;democratic
;executive;Germany;Ireland;United Kingdom
;executive;first
;United Kingdom;United Kingdom Parliament

;Apostolic;Catholic Church

;Roman;Republic




;Constitutionalism








;United States




;European
;Lisbon
;United Nations;Charter
;http://constituteproject.org
;Constitutionalism
;Constitutional Law
;http://www.servat.unibe.ch/icl/;International Constitutional Law:];English
;European Union
;United Nations
