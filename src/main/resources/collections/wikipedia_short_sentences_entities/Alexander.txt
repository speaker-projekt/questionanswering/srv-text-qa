;Alexander;Alexander the Great;king;Greek;Macedonia;one
;Alexander;Defender;Greek;November 2012
;Mycenaean;Greek;Alexandra;Alaksandu;Alakasandu;king;Wilusa;king;Muwatalli;1280 BC;Greek;Alexandros
;one;Greek;Hera;one;Iliad;Paris;Alexander
;Georg Autenrieth;Perseus Digital Library;Greek;King;Alexander III;Alexanders;him;June 2010
;Alexander;Scotland;Russia
;Alexander;Alexandros;Ilion;Paris
;Alexander;Corinth;10th;king;Corinth;816;791&nbsp;BC
;Alexander I of Macedon
;Alexander II of Macedon
;Alexander III of Macedon;Alexander the Great
;Alexander IV of Macedon
;Alexander V of Macedon
;Alexander;369;358&nbsp;BC
;Alexander;Epirus;king;Epirus;about 342&nbsp;BC
;Alexander II;Epirus;king;272&nbsp;BC
;Alexander;Corinth;Antigonus Gonatas;Corinth;250&nbsp;BC
;Alexander;220 BC;Persis;Seleucid;king;Antiochus III
;Alexander Balas;Seleucid;Syria;150;146&nbsp;BC
;Alexander Zabinas;Seleucid;Syria;Antioch;128;123&nbsp;BC
;Alexander Jannaeus;king;Judea;103;76&nbsp;BC
;Alexander;Judaea;king;Judaea
;Alexander Severus;208;235;Roman;emperor
;Julius Alexander;the 2nd century;Emesene
;Domitius Alexander;Roman;emperor;308
;Alexander;Emperor;912;913
;Alexander;Scotland;1078;1124
;Alexander II;Scotland;1198;1249
;Alexander Nevsky;1220;1263;Prince;Novgorod;Prince;Vladimir
;Alexander III;Scotland;1241;1286
;Nicholas Alexander;Wallachia;Voivode;Wallachia;-1364
;Ivan Alexander;Bulgaria;Bulgaria;14th
;Aleksandr Mikhailovich;Tver;Prince of Tver;Alexander;Grand Prince of Vladimir-Suzdal;Alexander II;1301;1339
;1338;1386;Prince;Podolia
;Sikandar Shah Miri;Sikandar Butshikan;Iconoclast;sixth;sultan;Shah Miri;Kashmir;1353;1413
;Sikandar Shah;Sultan;Bengal;1358;1390
;Alexander;Georgia;1412;1442
;Alexander II;Georgia;1483;1510
;Alexandru I Aldea;Wallachia;1431;1436
;Emperor;Ethiopia;1472;1494
;Alexander Jagiellon;Alexander;Poland;King;Poland;1461;1506
;Sikandar Shah II;Sultan;Bengal;1481
;Alexandru Lăpuşneanu;Moldavia;1499;1568
;Sikandar Shah;Gujarat;Gujarat;-1526
;Sikandar Shah Suri;Shah;Delhi;-1559
;Alexandru II Mircea;Prince;Wallachia;1529;1577
;Alexander;Russia;1777;1825;emperor;Russia
;Alexander II;Russia;1818;1881;emperor;Russia
;Alexander III;Russia;1845;1894;emperor;Russia
;Alexander Karađorđević;Prince;Serbia;1842;1858
;Alexander;Bulgaria;1857;1893;first;prince;Bulgaria
;Alexandru Ioan Cuza;first;prince;Romania;1859;1866
;Alexander I Obrenović;Serbia;1876;1903;king;Serbia
;Alexander;Prince of Lippe;1831;1905;prince;Lippe
;Alexander;Yugoslavia;1888;1934;first;king;Yugoslavia
;Alexander;Crown Prince of Yugoslavia;1945;Yugoslav
;1895;1961;king;Albanians
;Alexander;Greece;1893;1920;king;Greece
;Leka;Crown Prince;Albania;1939;2011;king;Albanians
;Willem-Alexander;King;Netherlands;1967;Queen;Beatrix;Prince;Claus
;Alexander;Judean Prince;one;Mariamne;his
;Alexander Helios;Ptolemaic;prince;one;Mark Anthony
;Alexander;Judean Prince;Alexander;Cappadocian;princess;Glaphyra
;Alexander;1418;Bulgarian;Ivan Shishman
;Prince;Alexander John;Wales;1871;Edward VII
;Prince;Alexandre;Belgium;1942;2009
;Prince;Alfred;Edinburgh;Gotha;1874;1899
;Olav;Norway;Prince;Alexander;Denmark;1903;1991
;Pope;Alexander;pope;97;105
;Alexander;Apamea;5th-century;bishop;Apamea
;Pope Alexander II;pope;1058;1061
;Pope Alexander III;pope;1159;1181
;Pope;Alexander IV;pope;1243;1254
;Pope;Alexander V;Peter Philarges;1339;1410
;Pope;Alexander VI;1492;1503;Roman;pope
;Pope Alexander VII;1599;1667
;Pope Alexander VIII;pope;1689;1691
;Alexander;Constantinople;bishop;314;337
;Alexander;Alexandria;Coptic Pope;Patriarch;Alexandria;313;and;328
;Pope Alexander II;Alexandria;Coptic Pope;702;729
;Alexander of Lincoln;bishop;Lincoln
;Alexander of Jerusalem
;Saint Alexander
;Alexander;Greece;Rome
;Alexander of Lyncestis;330 BC;Alexander the Great
;Alexander;Polyperchon;314 BC;Macedonia
;Alexander;general;3rd-century BC;commander;Antigonus III Doson
;Alexander;Athens;3rd-century BC;poet
;Alexander Aetolus;280 BC;poet;Alexandrian Pleiad
;Alexander;284;281 BC;Macedonian
;Alexander;270;240 BC;Greek
;Alexander;general;Aegira;220&nbsp;BC
;Alexander;Acarnania;191&nbsp;BC;Antiochus III the Great
;Alexander Isius;198;189 BC;Aetolian;commander
;Alexander Lychnus;early 1st-century BC;poet;historian
;Alexander Philalethes;1st century BC;physician
;Alexander Polyhistor;Greek;scholar;the 1st century BC
;Alexander of Myndus;Greek;writer
;Alexander of Aegae;philosopher;the 1st century AD
;Alexander;Cotiaeum;2nd-century;Greek;grammarian;tutor;Marcus Aurelius
;Alexander Numenius;2nd-century;Greek
;Alexander Peloplaton;2nd-century;Greek
;Alexander;105;170;Greek;leader
;Alexander;200;Greek;commentator;philosopher
;Alexander;Lycopolis;4th-century;author;early Christian
;Alexander;Jerusalem Temple;Sanhedrin;4:6
;Alexander of Hales;13th-century;Medieval;theologian
;Alexander;magician;1880;1954;magician
;1983;Brazilian;basketball player;Alexandre
;Alexander
;Alexander Aigner;1909;1988;Austrian;mathematician
;Alexander Vasilyevich Alexandrov;1883;1946;Russian;composer
;Alexander Argov;1914;1995;Russian;Israeli;composer
;Alexander Armah;1994;American;football player
;Alexander Armstrong;1970;British;comedian;singer
;Aleksandr Averbukh;1974;Israeli;pole vaulter
;Alex Baldock;1970;British;businessman
;Alec Baldwin;1958;American;actor
;Alexander Björk;1990;golfer
;Alexander Graham Bell;1847;1922;Scottish;inventor;first
;Aleksander Barkov;1995;Finnish;hockey player
;Alexander Calder;1898;1976;American;sculptor
;Alexander Davidovich;1967;Israeli;wrestler
;Alexander Day;British;18th-century
;Alex DeBrincat;1997;American;hockey player
;Aleksandar Djordjevic;1967;Serbian;basketball player
;Alexander Dubček;1921;1992;leader;Czechoslovakia;1968;1969
;Alexandre Dumas;1802;1870;French;writer
;Alexandre;Gustave Eiffel;1832;1923;French;civil engineer;architect;designer;Eiffel;Tower
;Alexander Lee Eusebio;1988;Alexander;Xander;South Korean;singer
;Alexander Exarch;1810;1891;Bulgarian;publicist;journalist;independent;Bulgarian
;Alex Ferguson;1941;football player;manager
;Alexander Fleming;1881;1955;Scottish
;Alexander Zusia Friedman;1897;1943;Polish;rabbi;activist;journalist
;Alex Galchenyuk;1994;American;hockey player
;Alexander Glazunov;1865;1936;Russian;composer
;Alexander Goldberg;Israeli;chemical engineer;President;Israel Institute of Technology
;Alexander Gomelsky;1928;2005;Russian;head coach;USSR;30&nbsp;years
;Aleksandr Gorelik;1945;2012;Soviet;skater
;Alexander Gould;1994;American;actor
;Alexandre Grothendieck;1928;2014;mathematician
;Alexander Gustafsson;1987;Swedish
;Alexander Haig;1924;2010;American;general;politician
;Alexander Hamilton;1755;1804;first;United States;Secretary of the Treasury;one;United States
;Alexander Hamilton Jr.;1786;1875;American;attorney;Alexander Hamilton
;Alexander Hamilton Jr.;1816;1889;James Alexander Hamilton;Alexander Hamilton
;Alexander Held;1958;German;actor
;Alex Higgins;1949;2010;Irish
;Alexander Hollins;1996;American;football player
;Alexander Holtz;2002;Swedish;hockey player
;Alex Horne;1978;British;comedian
;Alexander von Humboldt;1769;1859;Prussian;explorer
;Alex Kapranos;1972;Scottish;musician;author;songwriter;producer;Franz Ferdinand
;Aleksandar Katai;1991;Serbian;footballer
;Alexander Kerfoot;1994;Canadian;hockey player
;Alex Killorn;1989;Canadian;hockey player
;Aleksandr Kogan;1985/86;psychologist;scientist
;Alexander Korda;1893;1956;Hungarian;film director
;Alexander Levinsky;1910;1990;Canadian;hockey player
;Alexander Ivanovich Levitov;1835;1877;Russian;writer
;Alexander Lévy;1990;French;golfer
;Alexandre Lippmann;1881;1960;French
;Alexander Ludwig;1992;Canadian;actor
;Sandy" Lyle;1958;Scottish;golfer
;Alexander Lukashenko;1954;President;Belarus
;Alex Manninger;1997;Austrian;footballer
;Alessandro Manzoni;1785;1873;Italian;poet;novelist
;Alexander;Ali;Marpet;1993;American;football player
;Alexander Mattison;1998;American;football player
;Alexander McQueen;1969;2010;British;fashion designer
;Alexander Michel Melki;1992;Swedish-Lebanese;footballer
;Alexander Mirsky;1964;Latvian;politician
;Alessandro Moreschi;1858;1922;Italian;singer
;Aleksandr Nikolayev;1897;1957;Russian;painter
;Alexander Nikolov;1940;boxer
;Alexander Noren;1982;golfer
;Alexander Nylander;1998;Swedish;hockey player
;Alexander O'Neal;1953;American;singer
;Alexander Ovechkin;1985;Russian;hockey player
;Alexander Patch;1889;1945;American;general;World;War;II
;Alexander Pechtold;1965;Dutch;politician
;Alexander Penn;1906;1972;Israeli;poet
;Alexander Pichushkin;1974;Russian;serial killer
;Alex Pietrangelo;1990;Canadian;hockey player
;Alexander Piorkowski;1904;1948;German;Nazi;commandant;war crimes
;Alexander Ponomarenko;1964;Russian;businessman
;Alexander Pope;1688;1744;English;poet
;Alexander Popov;1971;Russian;swimmer
;Alexander Ptushko;1900;1973;Russian;film director
;Alexander Pushkin;1799;1837;Russian;writer
;Alexander Radulov;1986;Russian;hockey player
;Alexander Ragoza;1858;1919;Russian;general;World;War
;Alexander Rendell;1990;Thai;actor;singer
;Alex Rodriguez;1975;Major;League Baseball;3;AL MVP
;Alexander Rou;1906;1973;Russian;film director
;Alexander Rowe;1992;Australian;athlete
;Alexander Rudolph;Al McCoy;1894;1966;American;boxer
;Alexander Rybak;1986;Norwegian;artist;violinist
;Alexander Salkind;1921;1997;French;film producer
;Alexander Scriabin;1872;1915;Russian;composer;pianist
;Alexander Semin;1984;Russian;hockey player
;Alexander Shatilov;1987;Uzbek-Israeli;gymnast
;Alexander Theodore;Sasha;Shulgin;1925;2014;American;chemist;author
;Alexander Slastin;1942;Russian;actor
;Alexander Stafford;British;politician
;Alexander Suvorov;1730;1800;Russian;leader;Rymnik;Count;Prince;Italy;Generalissimo;Russian;Empire
;Alexander Skarsgård;1976;Swedish;actor
;Alexander McCall Smith;1948;Scottish;writer
;Aleksandr Solzhenitsyn;1918;2008;Russian;writer;Nobel laureate;Soviet
;Alexander Steen;1984;Swedish;hockey player
;Alexandre Texier;1999;French;hockey player
;Lex van Dam;1968;Dutch;trader
;Alexander Van der Bellen;1944;12th;President;Austria
;Alexander Varchenko;1949;Russian;mathematician
;Aleksander Veingold;1953;Estonian;Soviet;chess player;coach
;Alessandro Volta;1745;1827;Italian;physicist
;Alexander Wennberg;1994;Swedish;hockey player
;Alexander Wilson
;Alexandre Yersin;1863;1943;Swiss-French;doctor;explorer;bacteriologist;Black Death;Hong Kong;Yersinia Pestis;disease;him;his
;Alexander Yusuf;architect
;Alex Zanardi;1966;Italian;driver
;Alexander Zverev;1997;German;tennis player
;Alexander
;Albanian;Aleksandër
;Albanian
;Isikinidiri
;Arabic;Iskandar
;Armenian;Aleksandr
;Asturian;Alexandru;Xandru
;Basque;Alesander
;Belarusian
;Bengali
;Bosnian;Aleksandar
;Bulgarian
;Catalan;Alexandre;Aleixandre
;Chinese
;Croatian;Aleksandar
;Czech;Alexandr
;Danish;Aleksander;Alexander
;Dutch;Alexander
;Aleksandro
;Estonian;Aleksander
;English;Alexander
;Finnish;Aleksanteri
;French;Alexandre;Léandre;Alexis
;Galician;Alexandre
;Georgian
;German;Alexander
;Greek
;Mycenaean;Greek
;Greek
;Greek
;Modern;Greek
;Alekanekelo
;Aleksander

;Hungarian;Alexander
;Icelandic;Alexander
;Indonesian;Iskandar
;Irish;Alastar
;Italian;Alessandro
;Japanese
;Korean
;Alexander
;Latvian;Aleksandrs
;Lithuanian;Aleksandras
;Macedonian;Aleksandar
;Iskandar

;Syriac
;Greek
;Anglican
;Mongolian
;Norwegian;Aleksander;Alexander
;Sikandar
;Persian;Eskandar;Sikandar
;Polish;Aleksander
;Portuguese;Alexandre;Alexandro;Alessandro;Leandro
;Sikandar
;Romanian;Alexandru
;Russian;Aleksandr

;Scottish;Alasdair;Alastair;Alistair;Alister
;Serbian;Aleksandar
;Slovak;Alexander
;Slovene;Aleksander
;Spanish;Alejandro
;Swedish;Alexander
;Alejandro
;Thai
;Turkish
;Ukrainian;Olexander;Oleksandr
;Urdu;Sikandar
;Valencian;Alecsandro;Aleksandro;Aleixandre;Alexandre
;Vietnamese;Alexander
;Welsh;Alexander
;Aleksander
;Alex
;Alexandra
;Alexander
;Alexander

;Hera Alexandros;Greek;Hera
