;Ashes;England;Australia;recently;currently;British;The Sporting Times;Australia;1882;The Oval;first;English;English;Australia;1882;83;Australia;English;captain;Ivo Bligh;English
;England;two;three;Bligh;Melbourne;Florence Morphy;Bligh;a year;Australian;MCC;Bligh;1927;his
;Ashes;Bligh;Ashes;1998;99;Waterford Crystal;Ashes;MCC Museum;Australia;two;Australian;1988;Ashes;2006;07
;Ashes;five;England;Australia;once;every two years;71;Australia;33;England;32;six
;first;England;Australia;Melbourne;Australia;1877;ninth;1882;England;year;Australians;one;Oval;London;Australia;63;first;England;A. N. Hornby;38-run;101;second;Australia;55;60;Hugh Massie;122;England;85;Australians;bowler;Fred Spofforth;W. G. Grace;his;he;Spofforth;English;four;two;England;eight;his
;Ted Peate;England;ten;Peate;two;Harry Boyle;his;he;England;Boyle;Spofforth
;Peate;captain;Charles Studd;one;England;two centuries;he;his;his;Peate;Studd

;British;Australians;Englishmen;Saturday, 9 September;first






;Demon


;31 August;Charles Alcock-edited;Weekly
;2 September;Reginald Shirley Brooks;Sporting Times
;Ivo Bligh;1882;83;Australia;England;captain;Ashes;he;Australian;He;three-match;two-one;England;fourth;Australians;July 2013
;the 20 years;Bligh;Ashes;England;Australia;first;George Giffen;1899;his
;1903;Pelham Warner;Australia;he;Bligh;20 years;Australian;Warner;his;general;Australia;first;Ashes;Wisden Cricketers' Almanack;1905;Wisden;first;1922
;years;England;Australia;Ashes;1925;Annual


;Chapman;Hendren;Hobbs
;Gilligan;Woolley;Hearne



;one;Warner;1904;Australian;captain;M. A. Noble;1909;Australian;captain;W. M. Woodfull;1934
;one;one;Bligh;Darnley;1882;83;Darnley;1894;Victorian;Darnley;Florence Morphy;Third;1883;Ronald Willis;Joy Munns;Christmas 1882;English;William Clarke;Sunbury;Victoria;his;Clarke
;August 1926;Ivo Bligh;now;Darnley;Ashes;Morning;Central Hall;Westminster;He;he
;Ivo Bligh;Countess;Darnley;1930;his;London Times;Her
;Darnley;1921;Cobham Hall;Montague Grover;He;his
;1926;He;Brisbane;Courier
;1998;Darnley;82-year-old;layer;her;Australia;2006/7;MCC;now;95%;Channel Nine TV;25 November 2006;he;6
;six-line;fourth;Melbourne;Punch;1 February 1883


;Ivo
;Studds


;Barlow;Bates


;February 1883;Fourth;Ann Fletcher;Joseph Hines Clarke;Marion Wright;Dublin;Bligh;Darnley;1921;Illustrated London News;January 1921;Darnley;1927;Marylebone Cricket Club;his;MCC;first;1953;MCC;MCC;England;Australia;MCC Cricket Museum;1882
;Australia;first;1988;Australian;the second;2006/7;17 October 2006;Sydney;Tasmanian Museum;Art Gallery;21 January 2007
;the 1990s;Australia;Ashes;MCC Cricket Museum;MCC;2002;Bligh;Clifton;Darnley;Australia;MCC;his
;MCC;Waterford Crystal;1998;99
;Ashes
;1882;Australian;The Oval;Bligh;England;Australia;he;Australia;First;nine;two;England;Third;England;Ashes;2;1;fourth;United;Australian;XI;Australian;three;1882;83;Australia
;Bligh;English;the 1880s;1890s;recent years;first;five-Test;1894;95;England;four;the 1880s;23;seven
;1887;88;two;English;Australia;The 1890s;Australia;first;1882;2;1;1891;92;England;three;1896
;1894;95;England;First;Sydney;10;Australia;586;Syd Gregory;201;George Giffen;161;England;325;England;437;Australia;166;Bobby Peel;6;67;the second last day;Australia;113;2;64;overnight;next morning;two;Johnny Briggs;England;3;2;England;6;English;27;26.70;Tom Richardson;32;26.53
;1896;England;W. G. Grace;2;1;England
;Australia;1897;98;4;1;Harry Trott;Joe Darling;three;1899;1901;02;1902;one;His
;Five;1902;first;two;First;first;Edgbaston;376;England;Australia;36;Wilfred Rhodes;7/17;46;2;Australia;Third;and Fourth;Bramall Lane;Old Trafford;Old Trafford;Australia;3;Victor Trumper;104;hundred;the first day;his;England;one;263;48;5;Jessop;104;hundred;75 minutes;He;his;George Hirst;Rhodes;15;Rhodes;Hirst;Wilfred;him;thirteen;two
;Australian;Trumper;Warwick Armstrong;James Kelly;Monty Noble;Clem Hill;Hugh Trumble;Ernie Jones
;MCC;first;MCC;Australia;1903;04;England;Plum Warner;England;captain;his;his;Ashes;England;Australia;Ashes
;England;Australia;First;World;War;1914;Five;1905;and;1912;1905;England;captain;Stanley Jackson;2;0;five;Monty Noble;Australia;1907;08;1909;England;1911;12;four;one;Jack Hobbs;England;three centuries;Frank Foster;32;21.62;Sydney Barnes;34;22.88
;England;1912;Triangular Tournament;South Africa;Australian;Clem Hill;Victor Trumper;Warwick Armstrong;Tibby Cotter;Sammy Carter;Vernon Ransford
;war;Australia;first;two;Jack Gregory;Ted McDonald;English;Australia;England;first;eight;5;0;1920;1921;Warwick Armstrong
;Armstrong;England;1921;two;first;England;his;his
;England;one;15;war;1925
;1926;England;1;0;The Oval;Australia;first;22;Jack Hobbs;Herbert Sutcliffe;49;0;the end of the second day;27;overnight;next day;England;Hobbs;Sutcliffe;172;Hobbs;100;Sutcliffe;161;England;Australian;captain;Herbie Collins;him
;Australia;1926;Collins;Charlie Macartney;Warren Bardsley;Gregory;1928;29
;Donald Bradman;Australians;Jack Ryder;4;1;England;Wally Hammond;905;113.12;Hobbs;Sutcliffe;Patsy Hendren
;1930;Bill Woodfull;England
;Bradman;1930;974;139.14;his;he;1930;the few centuries;one;Australia;334;309;the;end of the first day;a century;he;Bradman;254;his;England;The Oval;hundred;Bradman;7/92;Percy Hornibrook;England;second;Australia;2;1;Clarrie Grimmett;29;31.89;Australia
;Australia;one;1930s;Bradman;Archie Jackson;Stan McCabe;Bill Woodfull;Bill Ponsford;England;1932;33;captain;Douglas Jardine
;Jardine;Harold Larwood;Bill Voce;Australian;his;Jardine;Bodyline;England;4;1;Australia;Anglo-Australian;MCC
;Jardine;6,000;Ashes
;Australians;Woodfull;England;manager;Pelham Warner;two;He;One;Australian;Larwood;bouncer;Australian;him
;1930s;the Second;World;War;Australia;war
;1934;Larwood;Voce;Jardine;MCC;Australia;MCC;1932;33;Larwood;Larwood;MCC;He
;Australia;1934;1953;the Second;World;War
;1930;1934;The Oval;Australia;first;701;first;Bradman;244;Ponsford;266;451;second;England;707-run;Australia;2;1;Woodfull;captain;Australia;he;his
;1936;37;Bradman;Woodfull;Australian;captain;first;two;Australia;He;Australians;Bradman;first;3;2;his
;1938;two;1;1;Australia;first;two;Third;Old Trafford;Australia;five;three days;Headingley;Fifth;The Oval;Len Hutton;364;England;903-7;Bradman;Jack Fingleton;Hutton;nine;Australia;579
;war;England;1946;47;1920;21;Australia;Bradman;now;Ray Lindwall;Keith Miller;Australia;3;0
;38;war;Bradman;28;Jack Ikin;England;Bradman;He;he;his;umpire;Australian;captain;187;he;his;Australia;First;Test;Ikin;one
;1948;Australia;4;0;one;Australian;Bradman;40;England;his;34;three;five;27;7
;Bradman;England;Second;and Fourth;Headingley;Headingley;Australia;404;the last day;seven-wicket
;1948;one;Bradman;Australia;Fifth;The Oval;four;100;his;Bradman;Eric Hollies;99.94;him
;Bradman;Australian;captain;Lindsay Hassett;4;1;1950;51;one-sided
;1953;England;The Oval;1;0;Headingley;one;English;captain;Len Hutton;Denis Compton;Peter May;Tom Graveney;Colin Cowdrey;Fred Trueman;Brian Statham;Alec Bedser;Jim Laker;Tony Lock;wicket-keeper;Godfrey Evans;Trevor Bailey
;1954;55;Australia;Frank Tyson;Statham;First;Hutton;Australia;England;3;1
;1956;Jim Laker;Old Trafford;68;191;19;20;Australian;Fourth Test;he;Australia;second;summer;two;Bradman;2;1;England
;England;Australia;4;0;1958;59;spinner;Richie Benaud;31;five-Test;Alan Davidson;24;19.00;Australian;Ian Meckiff;English;Australia
;1961;Australia;2;1;first;Ashes;England;13 years;Second;Battle of the Ridge;Australia;the final day;Fourth;Old Trafford;Richie Benaud;6-70;English
;four;the 1960s;1962;63;1964;1965;66;1968;decade;England;the 1960s;20;four;Australia;four;England;three;Australia;Bob Simpson;Bill Lawry;Simpson;Lawry
;the 1960s;England;Australia;first;West Indies;England;the mid-1960s;South Africa;two;apartheid;Australia;3;1;4;0;Australia;2;1;West Indies;1964;65;first;England
;1970;71;Ray Illingworth;England;2;0;Australia;John Snow;Geoffrey Boycott;John Edrich;7th;one;England;Lawry;Sixth;Australia;Lawry;his
;1972;2;2;England;Illingworth
;1974;75;England;Geoff Boycott;Australian;Jeff Thomson;Dennis Lillee;4;1;England;England;1975;0;1;captain;Tony Greig
;Australia;1977;Centenary Test;Ashes;storm;Kerry Packer;World Series Cricket;his;Test-playing;Australia;Packer;Australian;Cricket Board;ACB;WSC-contracted;Australian;English;Ashes;West Indies;1970s;West Indies;Australia;England;the 1990s
;Greig;England;Mike Brearley;captain;Australia;he;Boycott;Brearley;1977;3;0;5;1;Australian;1978;79;Allan Border;Australia;1978;79;his
;Brearley;1980;Ian Botham;1981;England;captain;Australia;1;0;first;two;Botham;Brearley;Third;Headingley;Australia;2;0;England;227;England;135;7;a second;356;Botham;149;130;Australia;111;Bob Willis;8;43;first;1894;95;Brearley;England;two;The Oval
;1982;83;Australia;Greg Chappell;captain;England;South African;Graham Gooch;John Emburey;Australia;2;0;three;England;Fourth;Test;3;70-run
;1985;David Gower;England;Gooch;Emburey;Tim Robinson;Mike Gatting;Australia;now;Allan Border;South African;Terry Alderman;England;3;1
;West Indies;the 1980s;England;Mike Gatting;captain;1986;87;his;Chris Broad;three;hundreds;Graham Dilley;England;2;1
;Australian;1989;Australian;the past;England;4;0;Allan Border;Mark Taylor;Merv Hughes;David Boon;Ian Healy;Steve Waugh;Ashes;England;now;once;David Gower;Fourth;England;South Africa;the following winter;three;Tim Robinson;Neil Foster;John Emburey;England
;Australia;the 1990s;2000s;general;England;1989;Australia;1990;–91,;1993;,;1994;–95,;1997;1998;99;2001;2002;03
;Australian;the early years;Boon;Taylor;Steve Waugh;Taylor;the mid-1990s;Steve Waugh;2001;the 1990s;Waugh;Mark;Australia;Glenn McGrath;Jason Gillespie;his;Ian Healy;the 1990s;Adam Gilchrist;2001;to;2006;07;the 2000s;Justin Langer;Damien Martyn;Matthew Hayden;Australia;Australian;Shane Warne;first;1993;Mike Gatting;the Century
;Australia;1989;and;2005;two;1989;87;Australia;England;86;74;2005;Australia;115;England;93;82;1989;2005;two;43;Australia;28;England;7;8;England;Ashes;First;1997;Australia
;England;2004;year;second;2005;Ashes;Richie Benaud;past;1894;95;1902
;First;Australia;four;England;Second;Test;2;Third;two;Australian;England;Fourth;Test;three;Australia;first;191;England;Ashes;first;18 years;first;1985
;Australia;2006;07;5;0;second;Ashes;Glenn McGrath;Shane Warne;Justin Langer;Damien Martyn
;2009;First;SWALEC;Cardiff;England;James Anderson;Monty Panesar;69;England;first;1934;1;0;Edgbaston;fourth;Headingley;Australia;80;England;Fifth;The Oval;197;Andrew Flintoff
;2010;11;Australia;First;Brisbane;England;Second;Adelaide;71;Australia;Perth;Third;Fourth;Melbourne;Cricket Ground;England;second;513;Australia;98;258;157;England;2;1;England;3;1;Australia;83;Sydney;Fifth;1938;644;England;first;Australian;24 years;2010;11;one;three;first;England;500;four
;Australia;2013;Darren Lehmann;coach;Mickey Arthur;the previous year;captain;Ricky Ponting;Mike Hussey;opener;David Warner;England;First;14;19-year-old;Ashton Agar;98;11;first;England;one-sided;Second;347;Third;Old Trafford;England;England;Fourth;Test;74;Australia;eight;86;England;3;0
;the second;two;Ashes;2013;2014;Australia;five;third;Australia;England;six;Australian;specialist;Englishman;10 centuries;Ben Stokes;a century;England;Mitchell Johnson;37;English;13.97;Ryan Harris;22;19.31;5-Test;Stuart Broad;Stokes;England;spinner;Graeme Swann;Third
;Australia;2015;England;England;first;Cardiff;Australia;second;two;Australian;136;first;Edgbaston;England;eight;Australia;60;Stuart Broad;five;8;15;first;Trent Bridge;first;78;the morning of the third day;Fourth;England
;2017;18;Australia;captain;Steve Smith;vice-captain;David Warner;England;England;Ben Stokes
;Australia;first;Brisbane;10;second;Adelaide;120;first;Ashes;Australia;41;third;Perth
;2019;attacks;Australia;David Warner;Steve Smith;Cameron Bancroft;9;12 months;South Africa;India;first;Australia;Australia;Sri Lanka;2;0
;Cricket World Cup;July 2019;first;England;opener;Alastair Cook;August 2018;Rory Burns;Joe Denly;Jason Roy;2;1;West Indies;England;one-off;Ireland;143;2019;2;2;Australia
;Ashes;1882
;the 136 years since 1883;Australia;80.5 years;England;55.5 years
;2019;England
;4
;2019
;4
;five;four-match;1938;1975;six-match;1970;71;1974;75;1978;79;1981;1985;1989;1993;1997;Australians;264 centuries;23;200;Englishmen;212 centuries;10;200;Australians;10;41;Englishmen;38;August 2013
;United Kingdom;Australia;five
;Australia;currently;Brisbane;first;England;Australia;1932;33;Adelaide;Oval;1884;85;Melbourne;1876;77;Sydney;1881;82;Brisbane;Exhibition Ground;1928;29;Melbourne;Boxing;Day;Test;Sydney;New Year;Test;Perth;1970;71;2017;18;Perth;Stadium;2021;22
;Australia;2010;11;six;Bellerive Oval;Hobart;England;Wales Cricket Board;five
;England;Wales;currently;Old Trafford;Manchester;1884;The Oval;Kennington;South;London;1884;St John's Wood;North;London;1884;Headingley;Leeds;1899;Edgbaston;Birmingham;1902;Sophia Gardens;Cardiff;Wales;2009;Riverside;Ground;Chester-le-Street;County;Durham;2013;Trent Bridge;West Bridgford;Nottinghamshire;1899;one;Bramall Lane;Sheffield;1902;Oval;Sophia Gardens;Riverside;the years of 2020;2024;2027;Trent Bridge;host;2019;2023
;Ashes;England;Australia;Great Britain;Australia;Ashes;Ashes;Australian;two;1908;Sale of the Century;Australian;English
;Australia;the first half of the twentieth century;Australia;England;Australian;Ashes;Australian;the 1940s;Australia;New Zealand;Australasia;the 1950s;two;1923;Queensland;New South Wales;Australia;two;Western;Australia;annual;Great Southern Football Carnival;Ashes;today;Australia;England
;1953;Terence Rattigan;Jack Warner;England;cricketer;Ashes;English;captain;Len Hutton;England;1953;his
;Douglas Adams;1982;third;Guide;Galaxy;Wikkit Gate
;Bodyline;1932;33;Australia;1984;Gary Sweet;Donald Bradman;England;captain;Douglas Jardine
;1877;to;1883
;1884;to;1889
;1890;to;1900
;Willis;http://www.lutterworth.com/product_info.php?products_id=486;Lutterworth Press;1987;978;-0-7188-2588-1

;https://www.youtube.com/watch?v=toukmRyDuOE;the first hundred years;John Arlott
;http://www.cricinfo.com/engvaus2009/content/story/259985.html
;http://www.mcc.org.au/News/Club%20Publications/~/media/Files/Origin%20of%20the%20Ashes.ashx;Rex Harcourt
;http://aso.gov.au/titles/spoken-word/1930-australian-xi-ashes/;Don Bradman;1930
