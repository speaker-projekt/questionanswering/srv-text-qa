{{short description|Phylum of Excavata}}
{{Automatic taxobox
| image = Naegleria (formes).png
| image_caption = The three different stages of ''[[Naegleria Fowleri|N. fowleri]]''
| taxon = Percolozoa
| display_parents = 4
| authority = [[Thomas Cavalier-Smith|Cavalier-Smith]] 1991
| subdivision_ranks = Classes, Orders and Families
| subdivision = 
* Class '''Heterolobosea'''
*:<small>Page & Blanton 1985</small>
** Order [[Schizopyrenida]]
*** Family [[Vahlkampfiidae]]
*** Family [[Gruberellidae]]
** Order [[Acrasida]]
*** Family [[Acrasidae]]
** ''Incertae sedis''
*** Family [[Lyromonadidae]]
* Class [[Percolatea]]
}}
The '''Percolozoa''' are a group of colourless, non-photosynthetic [[Excavata]], including many that can transform between [[amoeboid]], [[flagellate]], and [[microbial cyst|cyst]] stages.

==Characteristics==
Most Percolozoa are found as bacterivores in soil, fresh water and occasionally in the ocean. The only member of this group that is infectious to humans is ''[[Naegleria fowleri]]'', the causative agent of the often fatal disease [[amoebic meningitis]]. The group is closely related to the [[Euglenozoa]], and share with them the unusual characteristic of having [[mitochondrion|mitochondria]] with discoid [[crista]]e.  The presence of a ventral feeding groove in the flagellate stage, as well as other features, suggests that they are part of the [[Excavata]] group.

The amoeboid stage is roughly cylindrical, typically around [[1 E-5 m|20-40 μm]] in length.  They are traditionally considered lobose amoebae, but are not related to the others, and unlike them, do not form true lobose [[pseudopods]].  Instead, they advance by eruptive waves, where hemispherical bulges appear from the front margin of the cell, which is clear.  The flagellate stage is slightly smaller, with two or four anterior flagella anterior to the feeding groove.

Usually, the amoeboid form is taken when food is plentiful, and the flagellate form is used for rapid locomotion.  However, not all members are able to assume both forms.  The genera ''[[Percolomonas]]'', ''[[Lyromonas]]'', and ''[[Psalteriomonas]]'' are known only as flagellates, while ''[[Vahlkampfia]]'', ''[[Pseudovahlkampfia]]'', and most [[acrasid]]s do not have flagellate stages.  As mentioned above, under unfavourable conditions, the acrasids aggregate to form sporangia.  These are superficially similar to the sporangia of the [[dictyostelid]]s, but the amoebae only aggregate as individuals or in small groups and do not die to form the stalk.

==Terminology and classification==
These are collectively referred to as schizopyrenids, amoeboflagellates, or vahlkampfids.  They also include the acrasids, a group of social amoebae that aggregate to form [[sporangium|sporangia]].  The entire group is usually called the '''Heterolobosea''', but this may be restricted to members with amoeboid stages.

One Heterolobosea classification system is:<ref name="urlHeterolobosea">{{cite web |url=http://tolweb.org/Heterolobosea/96360 |title=Heterolobosea |access-date=2009-03-25}}</ref>
* Order [[Schizopyrenida]]
** Family [[Vahlkampfiidae]]
** Family [[Gruberellidae]]
* Order [[Acrasida]]
** Family [[Acrasidae]]
''Pleurostomum flabellatum'' has recently been added to Heterolobosea.<ref name="pmid17576098">{{cite journal |vauthors=Park JS, Simpson AG, Lee WJ, Cho BC |title=Ultrastructure and phylogenetic placement within Heterolobosea of the previously unclassified, extremely halophilic heterotrophic flagellate Pleurostomum flabellatum (Ruinen 1938) |journal=Protist |volume=158 |issue=3 |pages=397–413 |date=July 2007 |pmid=17576098 |doi=10.1016/j.protis.2007.03.004 }}</ref>

===Phylogeny===
Based on the cladogram from Tolweb and updated with new data from Pánek & Čepička 2012{{Citation needed|date=December 2019|reason=removed citation to predatory publisher content}} and Pánek, Ptackova & Čepička  2014.<ref>{{cite journal |authors=Pánek, Ptackova & Čepička |title=Survey on diversity of marine/saline anaerobic Heterolobosea (Excavata: Discoba) with description of seven new species |journal=International Journal of Systematic and Evolutionary Microbiology |volume=64|issue= Pt 7|pages=2280–2304 |doi=10.1099/ijs.0.063487-0 |date=2014  |url=http://ijs.sgmjournals.org |pmid=24729392}}</ref>

{{clade| style=font-size:90%;line-height:80%
|label1=
|1={{Clade
  |label1=[[Pharyngomonadea]]
  |1={{Clade
    |1=''[[Pharyngomonas]]''
     }}
  |label2=[[Tetramitia]]
  |2={{Clade
    |label1=[[Selenaionida]]
    |1={{Clade
      |1=[[Selenaionidae]]
       }}
    |2={{Clade
      |label1=[[Neovahlkampfiida]]
      |1=''[[Neovahlkampfia]]''
      |2={{Clade
        |label1=[[Lyromonadea]]
        |1={{Clade
          |label1=[[Paravahlkampfiidae]]
          |1={{Clade
            |1=''[[Fumarolamoeba]]''
            |2=''[[Paravahlkampfia]]''
             }}
          |label2=[[Lyromonadida]]
          |2={{Clade
            |label1=[[Plaesiobystridae]]
            |1={{Clade
              |1=''[[Euplaesiobystra]]''
              |2=''[[Heteramoeba]]''
               }}
            |2={{Clade
              |label1=[[Gruberellidae]]
              |1={{Clade
                |1=''[[Vrihiamoeba]]''
                |2={{Clade
                  |1=''[[Oramoeba]]''
                  |2=''[[Stachyamoeba]]''
                   }}
                 }}
              |label2=[[Psalteriomonadidae]]
              |2={{Clade
                |1=''[[Pseudoharpagon]]''
                |2={{Clade
                  |1={{Clade
                    |1=''[[Sawyeria]]''
                    |2=''[[Psalteriomonas]]''
                     }}
                  |2={{Clade
                    |1=''[[Pseudomastigamoeba]]''
                    |2={{Clade
                      |1=''[[Harpagon]]''
                      |2=''[[Monopylocystis]]''
                       }}
                     }}
                   }}
                 }}
               }}
             }}
           }}
        |label2=[[Heterolobosea]]
        |2={{Clade
          |label1=[[Acrasida]]
          |1={{Clade
            |1={{Clade
              |1=''[[Allovahlkampfia]]''
              |2=''[[Solumitrus]]''
               }}
            |2={{Clade
              |1=''[[Pocheina]]''
              |2=''[[Acrasis]]''
               }}
             }}
          |label2=[[Schizopyrenida]] s.s.
          |2={{Clade
            |label1=[[Naegleriidae]]
            |1={{Clade
              |1={{Clade
                |1=''[[Pleurostomum]]''
                |2=''[[Tulamoeba]]''
                 }}
              |2={{Clade
                |1=''[[Marinamoeba]]''
                |2={{Clade
                  |1=''[[Naegleria]]''
                  |2=''[[Willaertia]]''
                   }}
                 }}
               }}
            |2={{Clade
              |label1=[[Vahlkampfiidae]] s.s.
              |1={{Clade
                |1=''[[Tetramitus]]''
                |2=''[[Vahlkampfia]]''
                 }}
              |label2=[[Percolatea]]
              |2={{Clade
                |1=''[[Percolomonas]]''
                |2=''[[Stephanopogon]]''
                 }}
               }}
             }}
           }}
         }}
       }}
     }}
   }}
}}

===Taxonomy===
Phylum Percolozoa <small>Cavalier-Smith 1991</small><ref>{{cite journal | website=Collection of genus-group names in a systematic arrangement | title=Part 1- Virae, Prokarya, Protists, Fungi | url=http://mave.tweakdsl.nl/tn/genera1.html | display-authors=etal | access-date=30 June 2016 | archive-url=https://web.archive.org/web/20160814234049/http://mave.tweakdsl.nl/tn/genera1.html | archive-date=14 August 2016 | url-status=dead }}</ref>
* Subphylum [[Pharyngomonada]]
** Class [[Pharyngomonadea]] [Macropharyngomonadidea]
*** Order [[Pharyngomonadida]] [Macropharyngomonadida]
**** Family [[Pharyngomonadidae]] <small>Cavalier-Smith 2008</small> [Macropharyngomonadidae <small>Cavalier-Smith 2008</small>]
***** Genus ''[[Pharyngomonas]]''  <small>Cavalier-Smith 2008</small> [''[[Macropharyngomonas]]'' nomen nudum]
* Subphylum [[Tetramitia]] <small>Cavalier-Smith 1993 emend. Cavalier-Smith 2008</small>
** Genus ?''[[Costiopsis]]'' <small>Senn 1900</small>
** Genus ?''[[Hoehnmastix]]'' <small>Skvortzov 1974</small>
** Genus ?''[[Planiosculum]]'' <small>Szabados 1948</small>
** Genus ?''[[Protomyxomyces]]'' <small>Cunningham 1881</small>
** Genus ?''[[Protonaegleria]]'' <small>Michel & Raether 1985</small>
** Genus ?''[[Pseudovahlkampfia]]'' <small>Sawyer 1980</small>
** Genus ?''[[Schizamoeba]]'' <small>Davis 1926</small>
** Genus ?''[[Tetramastigamoeba]]'' <small>Singh & Hanumaiah 1977</small>
** Genus ?''[[Trimastigamoeba]]'' <small>Whitmore 1911</small>
** Genus ?''[[Wasielewskia]]'' <small>Hartmann & Schuessler 1913</small>
** Order ?[[Euhyperamoebida]]
*** Family [[Euhyperamoebidae]] <small>Goodkov & Seravin 1984</small> [Hyperamoebidae <small>Goodkov, Seravin & Railkin 1982</small>]
**** Genus ''[[Euhyperamoeba]]'' <small>Goodkov & Seravin 1984</small> [''[[Hyperamoeba]]'' <small>Goodkov, Seravin & Railkin 1982 non Alexeieff 1923</small>]
** Order [[Selenaionida]] <small>Hanousková, Táborský & Čepička 2018</small>
*** Family [[Selenaionidae]] <small>Hanousková, Táborský & Čepička 2018</small>
**** Genus ''[[Selenaion koniopes]]'' <small>Park, De Jonckheere & Simpson 2012</small>
**** Genus ''[[Dactylomonas]]'' <small>Hanousková, Táborský & Čepička 2018</small>
** Order [[Neovahlkampfiida]]
*** Family [[Neovahlkampfiidae]]
**** Genus ''[[Neovahlkampfia]]'' <small>Brown & de Jonckheere 1999</small>
** Class [[Lyromonadea]] <small>Cavalier-Smith 1993</small>
*** Order [[Paravahlkampfiida]]
**** Family [[Paravahlkampfiidae]]
***** Genus ''[[Fumarolamoeba]]'' <small>De Jonckheere, Murase & Opperdoes 2011</small>
***** Genus ''[[Paravahlkampfia]]'' <small>Brown & de Jonckheere 1999</small>
*** Order [[Lyromonadida]] <small>Cavalier-Smith 1993</small>
**** Family [[Lyromonadidae]] <small>Cavalier-Smith 1993</small>
***** Genus ''[[Lyromonas]]'' <small>Cavalier-Smith 1993</small>
**** Family [[Plaesiobystridae]]
***** Genus ''[[Pernina]]'' <small>El Kadiri, Joyon & Pussard 1992</small>
***** Genus ''[[Euplaesiobystra]]'' <small>Park et al. 2009</small>
***** Genus ''[[Plaesiobystra]]''
***** Genus ''[[Heteramoeba]]'' <small>Droop 1962</small>
**** Family [[Gruberellidae]] <small>Page & Blanton 1985</small>
***** Genus ''[[Gruberella]]'' <small>Page 1984 non Gruber 1889 non Corliss 1960</small>
***** Genus ''[[Vrihiamoeba]]'' <small>Murase, Kawasak & Jonckheere 2010</small>
***** Genus ''[[Oramoeba]]''
***** Genus ''[[Stachyamoeba]]'' <small>Page 1975</small>
**** Family [[Psalteriomonadidae]] <small>Cavalier-Smith 1993</small>
***** Genus ''[[Pseudoharpagon]]'' <small>Panek et al. 2012</small>
***** Genus ''[[Sawyeria]]'' <small>O'Kelly et al. 2003</small>
***** Genus ''[[Psalteriomonas]]'' <small>Broers et al. 1990</small>
***** Genus ''[[Pseudomastigamoeba]]''
***** Genus ''[[Harpagon]]'' <small>Panek et al. 2012</small>
***** Genus ''[[Monopylocystis]]'' <small>O'Kelly et al. 2003</small>
** Class [[Heterolobosidea]]
*** Order [[Acrasida]] <small>Schröter 1886</small>
**** ?Genus ''[[Allovahlkampfia]]'' <small>Walochnik & Mulec 2009</small>
**** ?Genus ''[[Solumitrus]]'' <small>Wang et al. 2011</small>
**** Family [[Acrasidae]] <small>Poche 1913</small>
***** Genus ''[[Acrasis]]'' <small>van Tieghem 1880</small>
***** Genus ''[[Pocheina]]'' <small>Loeblich & Tappan 1961</small>
**** Family ?[[Guttulinopsidae]] <small>Olive 1970</small> [Guttulinidae <small>Berl. 1888</small>; Guttulinopsaceae]
***** Genus ''[[Guttulina]]'' <small>Cienkowski 1873 non D’Orbigny 1839</small>
***** Genus ''[[Guttulinopsis]]'' <small>Olive 1901 </small>
***** Genus ''[[Rosculus]]'' <small>Hawes 1963</small>
*** Order [[Schizopyrenida]] <small>Singh 1952 s.s.</small>
**** Family [[Naegleriidae]] [Tulamoebidae <small>Kirby et al. 2015</small>]
***** Genus ''[[Aurem]]'' <small>Jhin & Park 2018</small>
***** Genus ''[[Pleurostomum]]'' <small>Namyslowski 1913</small>
***** Genus ''[[Tulamoeba]]'' <small>Park et al. 2009</small>
***** Genus ''[[Marinamoeba]]'' <small>De Jonckheere et al. 2009</small>
***** Genus ''[[Willaertia]]'' <small>de Jonckheere et al. 1984</small>
***** Genus ''[[Naegleria]]'' <small>Aléxéieff 1912</small> [''[[Trimastigamoeba]]'' <small>Whitmore 1911</small>; ''[[Didascalus]]'' <small>Singh 1952</small>]
**** Family [[Vahlkampfiidae]] <small>Jollos 1917 s.s.</small> [Tetramitaceae]
***** Genus ''[[Tetramitus]]'' <small>Perty 1852</small> [''[[Copromastix]]'' <small>Aragao 1916</small>; ''[[Adelphamoeba]]'' <small>Napolitano, Wall & Ganz 1970</small>, ''[[Learamoeba]]'' <small>Sawyer et al. 1998</small>, ''[[Paratetramitus]]'' <small>Darbyshire, Page & Goodfellow 1976</small>, ''[[Singhamoeba]]'' <small>Sawyer, Nerad & Munson 1992</small>; ''[[Schizopyrenus]]'' <small>Singh 1952</small>]
***** Genus ''[[Vahlkampfia]]'' <small>Chatton & LaLung-Bonnaire 1912</small>
**** Family [[Percolomonadidae]] <small>Cavalier-Smith 1993</small> [Choanogasteraceae]
***** Genus ''[[Percolomonas]]'' <small>Fenchel & Patterson 1986</small> [''[[Choanogaster]]'' <small>Pochmann 1959</small>]
**** Family [[Stephanopogonidae]] <small>Corliss 1961</small>
***** Genus ''[[Stephanopogon]]'' <small>Entz 1884</small>

==History==
The Heterolobosea were first defined by [[F. C. Page|Page]] and [[R. L. Blanton|Blanton]] in 1985<ref>
{{cite journal
 | last = Page | first =  F.C.
 |author2=R.L. Blanton
 | title = The Heterolobosea (Sarcodina: Rhizopoda), a new class uniting the Schizopyrenida and Acrasidae (Acrasida)
 | journal = Protistologica
 | year = 1985 | volume = 21 | pages = 121–132
 }}
</ref> as a class of amoebae, and so only included those forms with amoeboid stages.  [[Thomas Cavalier-Smith|Cavalier-Smith]] created the phylum Percolozoa for the extended group, together with the enigmatic flagellate ''[[Stephanopogon]]''.<ref>{{cite book
 | last = Cavalier-Smith | first = T.
 | year = 1991
 | chapter = Cell diversification in heterotrophic flagellates
 | title = The Biology of Free-living Heterotrophic Flagellates
 | editor = D.J. Patterson & J. Larsen
 | pages = 113–131
 | publisher = Oxford University Press
 }}
</ref>

Cavalier-Smith maintained the Heterolobosea as a class for amoeboid forms. He has defined Percolozoa as "Heterolobosea plus [[Percolatea]] classis nov."<ref name="pmid14657102">{{cite journal |author=Cavalier-Smith T |title=The excavate protozoan phyla Metamonada Grassé emend. (Anaeromonadea, Parabasalia, Carpediemonas, Eopharyngia) and Loukozoa emend. (Jakobea, Malawimonas): their evolutionary affinities and new higher taxa |journal=Int. J. Syst. Evol. Microbiol. |volume=53 |issue=Pt 6 |pages=1741–58 |date=November 2003 |pmid=14657102 |doi= 10.1099/ijs.0.02548-0|url=http://ijs.sgmjournals.org/cgi/pmidlookup?view=long&pmid=14657102|doi-access=free }}</ref>

==References==
{{Reflist}}

==External links==
* [http://tolweb.org/Heterolobosea/96360 Tree of Life Heterolobosea]

{{Excavata}}
{{Taxonbar|from=Q22094454}}

[[Category:Percolozoa| ]]
[[Category:Bikont phyla]]
[[Category:Taxa named by Thomas Cavalier-Smith]]