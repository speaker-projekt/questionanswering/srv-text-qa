{{Infobox software
| name = XyWrite
| logo = <!-- Image name is enough. -->
| logo alt = 
| logo caption = 
| screenshot = <!-- Image name is enough. -->
| screenshot alt = 
| caption = 
| collapsible = <!-- Any text here will collapse the screenshot. -->
| author = David Erickson
| developer = 
| released = {{Start date and age|1984}}<!-- {{Start date and age|YYYY|MM|DD|df=yes/no}} --><ref name="malloy198410">{{cite news | url=https://archive.org/stream/byte-magazine-1984-10/1984_10_BYTE_09-11_Databases#page/n243/mode/2up | title=Reviewer's Notebook | work=BYTE | date=October 1984 | access-date=23 October 2013 | author=Malloy, Rich | pages=245}}</ref>
| discontinued = <!-- Set to yes if software is discontinued, otherwise omit. -->
| ver layout = <!-- simple (default) or stacked -->
| latest release version = [[MS-DOS]], 4.18 (1993)<br />[[Windows]], 4.13
| latest release date = <!-- {{Start date and age|YYYY|MM|DD|df=yes/no}} -->
| latest preview version = 
| latest preview date = <!-- {{Start date and age|YYYY|MM|DD|df=yes/no}} -->
| repo = <!-- {{URL|example.org}} -->
| programming language = 
| operating system = [[MS-DOS]], [[Microsoft Windows|Windows]]
| platform = 
| size = 
| language = 
| language count = <!-- Number only -->
| language footnote = 
| genre = [[Word processor]]
| license = 
| alexa = 
| website = <!-- {{URL|example.org}} -->
| standard = 
| AsOf = 
}}
{{refimprove|section|date=September 2018}}
'''XyWrite''' is a [[word processor]] for [[MS-DOS]] and [[Microsoft Windows|Windows]] modeled on the mainframe-based [[Atex (software)|ATEX typesetting system]].<ref>{{citation|title=PERIPHERALS; Mastering XyWrite|year =1988|work =[[New York Times]]}}</ref> Popular with writers and editors for its speed and degree of customization, XyWrite was in its heyday the house word processor in many editorial offices, including the ''[[New York Times]]'' from 1989 to 1993.<ref name=":0">{{cite web|last1=Baehr|first1=Tim|title=Whatever became of XyQuest?|url=http://yesss.freeshell.org/x/_xyq.htm|access-date=13 May 2018}}</ref> XyWrite was developed by David Erickson and marketed by [[XyQuest]] from 1982 through 1992, after which it was acquired by The Technology Group. The final version for MS-DOS was 4.18 (1993); for Windows, 4.13.

==Features==
* Its file format consists of plain text ([[Codepage 437|IBM437]], or so-called "extended [[ASCII]]") with markup (within [[guillemet]]s: «&nbsp;»). This capability is useful for [[typesetter]]s who need to convert to various formats, e.g., [[LaTeX]]. A plug-in for ANSI characters is available.
* XyWrite is written in [[assembly language]], allowing it to run faster than word processors written in a higher level language.{{Citation needed|reason=This assumption may or may not be true or even relevant given the application|date=December 2014}}
* It has a flexible [[macro (computer science)|macro]]-programming language (XPL) that offers many advantages for quick [[search and replace]], copy-editing and reformatting of raw text. Users continue to write and share macros extending XyWrite features (printing to [[USB]] devices, for example).
* Plain-text, editable [[configuration file]]s allow easy customization of the keyboard—for remapping keystrokes and for execution of complex commands with individual keystrokes—as well as customization of what is loaded on launching the program.
* Commands can be typed in directly on a [[command line]], without the use of a mouse. Commands are usually in simple English, such as "Save," "Print," and "Search," or their shorter versions, such as "Sa" for "Save" (commands are case-insensitive).
* Up to nine files can be opened for editing at one time in separate "windows" that allow quick copy-and-paste among several files. Two files may be opened on the same screen for easy comparison of changes; a XyWrite command will do the comparison automatically, putting the cursor on the location at which the two files first differ (from which the user can move to the next difference).
* Version 4 has full [[WYSIWYG]] graphical editing capabilities including on-screen display of [[bitmap]]s.

==History and current usage==
XyQuest was founded in June 1982 by former [[Atex (software)|ATEX]] employees Dave Erickson and John Hild. Its most successful product was XyWrite III Plus, which attracted a devoted following among professional writers.

The turning point for XyWrite came in the form of a disastrous near-partnership with [[IBM]], which was seeking a modern replacement for its venerable [[DisplayWrite]] word processor (this and the following paragraphs are based mainly on Baehr 2018<ref name=":0" />). Working under an agreement signed in June 1990, XyQuest devoted nearly all of its development resources to revising Erickson's XyWrite IV to IBM's specifications, including [[IBM Common User Access]]-style menus, mouse support and a [[graphical user interface]]. Envisioned as a marriage between XyQuest technology and IBM marketing, the product was to be called Signature.

But on the eve of Signature's release, IBM announced a strategic decision to withdraw completely from the desktop software market, shocking XyQuest and leaving Signature in limbo. When a prospective new alliance with [[Lotus Software|Lotus]] did not materialize, XyQuest had no alternative but to resticker the ready-to-ship Signature packages as XyWrite 4.0 and attempt to carry on.

However, the changes IBM had insisted on were a liability where the III Plus user base was concerned. Some key reviews (such as in ''[[The Wall Street Journal]]'') were harsh, and there were complaints that 4.0 was buggy and slow. Moreover, in the years since the last major XyWrite release, [[WordPerfect]] had cemented its hold on the DOS word processor market. Already financially strained by the long development cycle for Signature, by the end of 1992 XyQuest was bleeding money. The sale to The Technology Group ensued.

While there were a few maintenance releases of 4.0 after the acquisition, The Technology Group's major commitment was to developing XyWrite for Windows. But XyWrite remained a niche product, unable to compete for the business user against [[Word for Windows]], [[WordPerfect for Windows]], and [[Ami Pro]], despite added versatility and customization potential. The Technology Group was dissolved in 2003.

Several versions of XyWrite were also localized for use in European countries. For example, the programs were offered in Germany under the name "euroscript" by North American Software GmbH.<ref>"[https://www.heise.de/ct/artikel/Eine-kleine-Geschichte-der-Textverarbeitung-4558977.html Eine kleine Geschichte der Textverarbeitung]", C't.</ref>

===Nota Bene===
A descendant of XyWrite called [[Nota Bene (word processor)]] is still being actively developed. Nota Bene, which runs on the XyWrite engine, is popular among academics. As of January 2020, Nota Bene for Windows is at version 12. NotaBene is supported on native Windows, Mac and on Linux boxes running [[Wine (software)|WINE]].<ref>"[https://www.notabene.com/system_req.html System requirements]", Nota Bene</ref>

===Current Usage===
Thanks in large part to the work of users of XyWrite, the program is still very usable with Windows (or MS-DOS, and thus Linux<ref>Jim Hall, "[https://opensource.com/article/17/10/run-dos-applications-linux How to run DOS programs in Linux]", OpenSource, 19 October 2017</ref>). Even on Pentium and similar hardware, it remains noticeably faster than [[Microsoft Word|MS Word]] or [[OpenOffice.org]].{{Citation needed|date=May 2020}} 

In 2015, work started on using XyWrite within the VDos program shell in 32 and 64 bit windows.  This was successful in October 2016, resulting in an x86 PC and DOS emulator for Windows based on Jos Schaars’s vDos. Formerly known as vDos-lfn, vDosPlus allows XyWrite 4, XyWrite III+, and Nota Bene for DOS to run under the latest versions of Microsoft Windows (including 64-bit Windows). [[VdosPlus.org]]<ref>https://sourceforge.net/projects/vdosplus/</ref><ref>http://vdosplus.org/</ref> shows the various functions, and XyWWWeb  <ref>XyWWWeb http://xywrite.org/xywwweb/ |accessdate=15 October 2019 </ref> shows usage.

Despite these advantages in speed, XyWrite does not have as many features as Word or OpenOffice.org. For example, XyWrite is unaware of Windows ANSI or Unicode character sets and Nota Bene does not support languages (such as Chinese) that require double-byte characters.

==Reception==
''[[Byte (magazine)|Byte]]'' in 1984 stated "the XyQuest people have done an admirable job porting the editing part of the Atex system" to the IBM PC. While criticizing the documentation, it called XyWrite "extremely fast, powerful, compact, and flexible".<ref name="malloy198410" />

==Version history==
;MS-DOS
* '''XyWrite I'''
* '''XyWrite II'''
* '''XyWrite II Plus'''
* '''XyWrite III''', May 1990 - distributed on 5.25" HD floppy diskettes, and shipped with a 3-ring looseleaf manual in fabric-covered slipcase
* '''XyWrite III Plus'''
* '''XyWrite 4.0''', Jan 1993 - distributed on seven 3.5" HD floppy diskettes, and shipped with five bound manuals: ''Installation & Learning Guide'', ''Making the Transition'', ''Customization Guide'', ''Command Reference Guide'', and ''LAN Administrator's Guide'' (together weighing nearly 4.5 pounds)
;Windows
* '''XyWrite for Windows'''

==See also==
*[[Bitstream Speedo Fonts]]
*[[List of word processors]]

==References==
{{Reflist}}

==External links==
*[http://www.notabene.com/ Nota Bene] (corporate site)
*[http://www.xywrite.com/ XyWrite.com] A General XyWrite Resource by Brian Henderson

{{Word processors}}

{{DEFAULTSORT:Xywrite}}
[[Category:1984 software]]
[[Category:Windows word processors]]
[[Category:Word processors]]
[[Category:Assembly language software]]