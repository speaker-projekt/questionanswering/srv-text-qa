{{short description|Order of dicot flowering plants}}
{{Automatic taxobox
| taxon = Brassicales
| image = LookZonderLook-bloem-hr.jpg
| image_caption = ''[[Alliaria petiolata]]'', garlic mustard ([[Brassicaceae]])
| authority = [[Edward Bromhead|Bromhead]]<ref name=APGIII2009>{{cite journal |last=Angiosperm Phylogeny Group |year=2009 |title=An update of the Angiosperm Phylogeny Group classification for the orders and families of flowering plants: APG III |journal=Botanical Journal of the Linnean Society |volume=161 |issue=2 |pages=105–121 |doi=10.1111/j.1095-8339.2009.00996.x |doi-access=free }}</ref>
| subdivision_ranks = Families
| subdivision =
* [[Akaniaceae]]
* [[Bataceae]]
* [[Brassicaceae]]
* [[Capparaceae]]
* [[Caricaceae]]
* [[Cleomaceae]]
* [[Emblingiaceae]]
* [[Gyrostemonaceae]]
* [[Koeberliniaceae]]
* [[Limnanthaceae]]
* [[Moringa]]ceae
* [[Pentadiplandraceae]]
* [[Resedaceae]]
* [[Salvadoraceae]]
* [[Setchellanthaceae]]
* [[Tiganophytaceae]]
* [[Tovariaceae]]
* [[Tropaeolaceae]]
}}

The '''Brassicales''' (or '''Cruciales''') are an [[order (biology)|order]] of [[flowering plant]]s, belonging to the [[eurosids II]] group of [[dicotyledon]]s under the [[APG II]] system.<ref>{{cite journal |author=Angiosperm Phylogeny Group |year=2003 |title=An update of the Angiosperm Phylogeny Group classification for the orders and families of flowering plants: APG II |journal=[[Botanical Journal of the Linnean Society]] |volume=141 |issue=4 |pages=399–436 |doi=10.1046/j.1095-8339.2003.t01-1-00158.x|url=https://academic.oup.com/botlinnean/article-pdf/141/4/399/17028996/j.1095-8339.2003.t01-1-00158.x.pdf |author-link=Angiosperm Phylogeny Group }}</ref> One character common to many members of the order is the production of [[glucosinolate]] (mustard oil) compounds. Most systems of classification have included this order, although sometimes under the name '''Capparales''' (the name chosen depending on which is thought to have priority).<ref name="hall2002">{{cite journal |author=Jocelyn C. Hall, Kenneth J. Sytsma & Hugh H. Iltis |year=2002 |title=Phylogeny of Capparaceae and Brassicaceae based on chloroplast sequence data |journal=[[American Journal of Botany]] |volume=89 |issue=11 |pages=1826–1842 |pmid=21665611 |doi=10.3732/ajb.89.11.1826|doi-access=free }}</ref>

The order typically contains the following families:<ref name="Haston">{{cite journal |author1=Elspeth Haston |author2=James E. Richardson |author3=Peter F. Stevens |author4=Mark W. Chase |author5=David J. Harris |year=2007 |title=A linear sequence of Angiosperm Phylogeny Group II families |journal=[[Taxon (journal)|Taxon]] |volume=56 |issue=1 |pages=7–12 |doi=10.2307/25065731 |jstor=25065731 |url=http://www.ingentaconnect.com/content/iapt/tax/2007/00000056/00000001/art00002}}</ref>
* [[Akaniaceae]] – two species of turnipwood trees, native to Asia and eastern Australia
* [[Bataceae]] – salt-tolerant shrubs from America and [[Australasian realm|Australasia]]
* [[Brassicaceae]] – [[Mustard plant|mustard]] and [[cabbage]] family; may include the [[Cleomaceae]]
* [[Capparaceae]] – [[caper]] family, sometimes included in [[Brassicaceae]]
* [[Caricaceae]] – [[papaya]] family
* [[Cleomaceae]]<ref name=APGIII2009 />
* [[Gyrostemonaceae]] – several genera of small shrubs and trees endemic to temperate parts of Australia
* [[Koeberliniaceae]] – one species of thorn bush native to Mexico and the US Southwest
* [[Limnanthaceae]] – [[meadowfoam]] family
* [[Moringa]]ceae – thirteen species of trees from Africa and India
* [[Pentadiplandraceae]] – African species whose berries have two highly sweet tasting proteins
* [[Resedaceae]] – [[Mignonette (Reseda)|mignonette]] family
* [[Salvadoraceae]] – three genera found from Africa to Java
* [[Setchellanthaceae]]
* [[Tovariaceae]]
* [[Tropaeolaceae]] – [[Tropaeolum|nasturtium]] family

Under the [[Cronquist system]], the Brassicales were called the Capparales, and included among the "[[Dilleniidae]]".  The only families included were the [[Brassicaceae]] and [[Capparaceae]] (treated as separate families), the [[Tovariaceae]], [[Resedaceae]], and [[Moringaceae]].  Other taxa now included here were placed in various other orders.

[[File:Cleome (Spider Flower) in Gavi.jpg|thumb|upright|''[[Cleome hassleriana]]'']]

The families Capparaceae and Brassicaceae are closely related.  One group, consisting of ''[[Cleome]]'' and related genera, was traditionally included in the Capparaceae but doing so results in a [[paraphyletic]] Capparaceae.<ref name="hall2002" />  Therefore, this group is generally now either included in the Brassicaceae or as its own family, [[Cleomaceae]].<ref name="Haston" /><ref>{{cite journal |author=Jocelyn C. Hall, Hugh H. Iltis & Kenneth J. Sytsma |year=2004 |title=Molecular phylogenetics of core Brassicales, placement of orphan genera ''Emblingia'', ''Forchhammeria'', ''Tirania'', and character evolution |journal=[[Systematic Botany]] |volume=29 |issue=3 |pages=654–669 |doi=10.1600/0363644041744491 |s2cid=86218316 |url=http://www.botany.wisc.edu/sytsma/pdf/Hall2004.pdf |archive-url=https://wayback.archive-it.org/all/20110401041157/http://www.botany.wisc.edu/sytsma/pdf/Hall2004.pdf |url-status=dead |archive-date=2011-04-01 |access-date=2016-08-26 }}</ref>

In 20 April 2020, newly described monotypic species from [[Namibia]], namely, ''Tiganophyton karasense'' {{small|Swanepoel, F.Forest & A.E. van Wyk}} is placed under this order as a monotypic member of new family [[Tiganophytaceae]], which is closely related to [[Bataceae]], [[Salvadoraceae]] and [[Koeberliniaceae]].<ref>{{Cite journal|last1=Swanepoel|first1=Wessel|last2=Chase|first2=Mark W.|author-link2=Mark Wayne Chase|last3=Christenhusz|first3=Maarten J.M.|author-link3=Maarten J. M. Christenhusz|last4=Maurin|first4=Olivier|last5=Forest|first5=Félix|last6=van Wyk|first6=Abraham E.|author-link6=Abraham Erasmus van Wyk|year=2020|title=From the frying pan: an unusual dwarf shrub from Namibia turns out to be a new brassicalean family|journal=Phytotaxa|volume=439|number=3|pages=171–185|doi=10.11646/phytotaxa.439.3.1|doi-access=free}}</ref>

== References ==
{{Reflist|32em}}

== External links ==
*{{Commons category-inline|Brassicales}}

{{Angiosperm orders}}
{{Taxonbar|from=Q21904}}

[[Category:Brassicales| ]]
[[Category:Angiosperm orders]]


{{Brassicales-stub}}