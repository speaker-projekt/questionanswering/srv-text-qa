package de.dfki.speaker.qa.engine.questionprocessing;

import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import edu.stanford.nlp.pipeline.CoreDocument;
import edu.stanford.nlp.pipeline.CoreEntityMention;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;

public class QuestionEntitiesProcessor implements QuestionProcessor{

	private StanfordCoreNLP pipeline;
	
	public QuestionEntitiesProcessor() {
		Properties props = new Properties();
	    props.setProperty("annotators", "tokenize,ssplit,pos,lemma,ner");
		// set up pipeline
		pipeline = new StanfordCoreNLP(props);
	}
	
	public void processQuestion(ProcessedQuestion pq) {
		List<String> entities = new LinkedList<String>();
		String questionText = pq.getText().trim();
		CoreDocument doc = new CoreDocument(questionText);
		// annotate the document
		pipeline.annotate(doc);
		for (CoreEntityMention em : doc.entityMentions()) {
//			System.out.println("\tdetected entity: \t"+em.text()+"\t"+em.entityType());
			entities.add(em.text());
		}
//		System.out.println("---");
//		System.out.println("tokens and ner tags");
//		String tokensAndNERTags = doc.tokens().stream().map(token -> "("+token.word()+","+token.ner()+")").collect(Collectors.joining(" "));
//		System.out.println(tokensAndNERTags);
		pq.setEntities(entities);
	}

//	public static void main(String[] args) {
//		QuestionEntitiesProcessor qep = new QuestionEntitiesProcessor();
//		ProcessedQuestion pq = new ProcessedQuestion("This is an example text that we can find in Berlin, "
//				+ "or probably we can pay it with 100 dollars in the European Comission in Paris.");
//		qep.processQuestion(pq);
//		
//		System.out.println(pq.toString());
//	}
}
