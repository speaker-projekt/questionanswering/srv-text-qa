package de.dfki.speaker.qa.engine.searchengine;

import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SearchDocument {

	private String text;
	
	private String documentId;
	
	private String source;
	
	private String entities;
	
	private double score;
	
	public String toJSON() throws Exception {
		return new ObjectMapper().writeValueAsString(this);
	}


}
