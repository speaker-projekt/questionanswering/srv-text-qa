package de.dfki.speaker.qa.engine;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import de.dfki.speaker.qa.engine.answerextraction.Answer;
import de.dfki.speaker.qa.engine.answerextraction.AnswerExtraction;
import de.dfki.speaker.qa.engine.questionprocessing.ProcessedQuestion;
import de.dfki.speaker.qa.engine.questionprocessing.QuestionProcessing;
import de.dfki.speaker.qa.engine.searchengine.SearchDocument;
import de.dfki.speaker.qa.engine.searchengine.SearchEngine;
import de.dfki.speaker.qa.engine.searchengine.Searcher;

@Component
public class TextQAEngine {

    Logger logger = Logger.getLogger(TextQAEngine.class.getName());

    @Autowired
	QuestionProcessing questionProcessing;
	
	@Autowired
	SearchEngine searchEngine;

	@Autowired
	AnswerExtraction answerExtraction;
	
	public TextQAEngine() {
	}
	
	public List<Answer> search(String text) throws Exception{
		
		ProcessedQuestion processedQuestion = questionProcessing.processQuestion(text);

		List<SearchDocument> documents = searchEngine.searchDocuments(processedQuestion);
		if(documents==null) {
			logger.warn("Null Documents are returned when requesting searchEngine with text search.");
			//throw new LDDPException("Null Documents are returned when requesting indexEngine with text search.");
			return null;
		}
		
		List<Answer> answers = answerExtraction.extractAnswers(processedQuestion, documents);		
		if(answers==null) {
			logger.warn("Null answers are returned when requesting answerExtractorn with documents.");
			//throw new LDDPException("Null Documents are returned when requesting indexEngine with text search.");
			return null;
		}
		return answers;
	}
	
	public boolean addDocument(String text, Map<String, String> metadata) throws Exception {
		return searchEngine.addDocument(text, metadata);
	}
	public boolean addDocuments(List<String> texts, List<Map<String, String>> metadatas) throws Exception {
		return searchEngine.addDocuments(texts, metadatas);
	}
	
}
