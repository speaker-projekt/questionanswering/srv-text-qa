package de.dfki.speaker.qa.engine.searchengine;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.client.solrj.response.UpdateResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrInputDocument;
import org.apache.solr.common.params.MapSolrParams;
import org.springframework.stereotype.Component;

import de.dfki.speaker.qa.engine.questionprocessing.ProcessedQuestion;

@Component
public class SolrSearcher implements Searcher {

	Logger logger = Logger.getLogger(SolrSearcher.class);
	
	String solrUrl = "http://localhost:8983/solr";
	SolrClient client;	
	String standardCollection;
	
	public SolrSearcher() {
		standardCollection = "text-qa-speaker";
		client = new HttpSolrClient.Builder(solrUrl)
				.withConnectionTimeout(10000)
				.withSocketTimeout(60000)
				.build();
	}
	
	public List<SearchDocument> searchDocuments(ProcessedQuestion processedQuestion, List<SearchDocument> resultList) throws Exception {
		/**
		 * Search in Solr index.
		 */
		final Map<String, String> queryParamMap = new HashMap<String, String>();
		queryParamMap.put("q", processedQuestion.getText());
		//queryParamMap.put("q", "text:"+text);
		queryParamMap.put("fl", "id,docid,text");
		queryParamMap.put("sort", "id asc");
		MapSolrParams queryParams = new MapSolrParams(queryParamMap);

		QueryResponse response;
		try {
			response = client.query(standardCollection, queryParams);
		} catch (SolrServerException | IOException e) {
			e.printStackTrace();
			String msg = "An exception ocurred: "+e.getMessage();
			logger.error(msg);
			throw new Exception(msg);
		}
		final SolrDocumentList documents = response.getResults();

		/**
		 * Convert Solr results into document results.
		 */
		System.out.println("Found " + documents.getNumFound() + " documents");
		for(SolrDocument document : documents) {
			final String id = (String) document.getFirstValue("id");
			final String docid = (String) document.getFirstValue("docid");
			final String dtext = (String) document.getFirstValue("text");
			System.out.println("id: " + id + "; docid: " + docid + "; text: " + dtext);
			/**
			 * Convert Solr output to LDDocument
			 */
			SearchDocument sdoc = new SearchDocument();
			
			/**
			 * implement the conversion of the search fields into Search Document, based on the stored information of the source document
			 */
			sdoc.setDocumentId(docid);
			sdoc.setText(dtext);			
			resultList.add(sdoc);
		}
		return resultList;
	}

	@Override
	public boolean addDocuments(List<String> texts, List<Map<String, String>> metadatas) throws Exception {
		for (String text : texts) {
			final SolrInputDocument doc = new SolrInputDocument();
			//String id = UUID.randomUUID().toString();
			String id = Integer.toString(text.hashCode());
			doc.addField("id", id);
			doc.addField("docid", id);
			doc.addField("text", text);
			try {
				final UpdateResponse updateResponse = client.add(standardCollection, doc);
				// Indexed documents must be committed
				client.commit(standardCollection);
			} catch (SolrServerException | IOException e) {
				e.printStackTrace();
				String msg = "There was en exception ocurring: "+e.getMessage();
				logger.error(msg);
				throw new Exception(msg);
			}
		}
		return true;
	}

	@Override
	public boolean addDocument(String text, Map<String, String> metadata) throws Exception {
		final SolrInputDocument doc = new SolrInputDocument();
		//String id = UUID.randomUUID().toString();
		String id = Integer.toString(text.hashCode());
		doc.addField("id", id);
		doc.addField("docid", id);
		doc.addField("text", text);
		try {
			final UpdateResponse updateResponse = client.add(standardCollection, doc);
			// Indexed documents must be committed
			client.commit(standardCollection);
		} catch (SolrServerException | IOException e) {
			e.printStackTrace();
			String msg = "There was en exception ocurring: "+e.getMessage();
			logger.error(msg);
			throw new Exception(msg);
		}
		return true;
	}

}
