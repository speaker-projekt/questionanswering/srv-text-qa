package de.dfki.speaker.qa.engine.questionprocessing;

import java.util.LinkedList;
import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class QuestionProcessing {

	List<QuestionProcessor> processors;
	
	public QuestionProcessing(){
		processors = new LinkedList<QuestionProcessor>();
		processors.add(new QuestionTypeProcessor());
		processors.add(new QuestionEntitiesProcessor());
	}
	
	public ProcessedQuestion processQuestion(String text) {
		ProcessedQuestion pq = new ProcessedQuestion(text);				
		/**
		 * Process question and include relevant information in the PQ object.
		 */
		for (QuestionProcessor qp : processors) {
			qp.processQuestion(pq);
		}
		return pq;
	}

}
