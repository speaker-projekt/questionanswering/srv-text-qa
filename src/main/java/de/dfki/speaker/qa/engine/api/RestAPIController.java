package de.dfki.speaker.qa.engine.api;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import de.dfki.speaker.qa.engine.TextQAEngine;
import de.dfki.speaker.qa.engine.answerextraction.Answer;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;

@RestController
@RequestMapping("text-qa")
@SpringBootApplication(exclude = {
        org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration.class})
//,
//        org.springframework.boot.actuate.autoconfigure.security.servlet.ManagementWebSecurityAutoConfiguration.class}
//        )
@OpenAPIDefinition(/*servers = {@Server(url = "https://apis.lynx-project.eu/api/named-entity-recognition")},*/
info = @Info(
		title = "Text-based QA Module/Service",
		version = "0.0.1",
		description = "The text-based QA developed by DFKI for the Speaker project.",
		contact = @Contact(url = "Lynx-Project API Team",
		name = "Speaker DFKI Team",
		email = "speaker-project@dfki.de")))
//@SecurityScheme(
//	name = "authorizationCode",
//	type = SecuritySchemeType.OAUTH2,
//	in = SecuritySchemeIn.HEADER,
//	bearerFormat = "jwt",
//	flows = @OAuthFlows(
//	        authorizationCode = @OAuthFlow(
//	                authorizationUrl = "https://keycloak-secure-88-staging.cloud.itandtel.at/auth/realms/Lynx/protocol/openid-connect/auth",
//	                tokenUrl = "https://keycloak-secure-88-staging.cloud.itandtel.at/auth/realms/Lynx/protocol/openid-connect/token")
//	)
//)
public class RestAPIController {

	Logger logger = Logger.getLogger(RestAPIController.class);

	@Autowired
	TextQAEngine engine;

	@PostConstruct
	public void setup() {
		try {
			String collectionPath = "src/main/resources/collections/wikipedia_short_sentences/";
			String entitiesPath = "src/main/resources/collections/wikipedia_short_sentences_entities/";
			File folder = new File(collectionPath);
			File [] files = folder.listFiles();
			int cnt = 1;
//			System.out.println(folder.getAbsolutePath());
			for (File file : files) {
				System.out.print(file.getName() + "...");
				String text = FileUtils.readFileToString(file,"utf-8");
				HashMap<String,String> metadata = new HashMap<String, String>();
				metadata.put("id", cnt+"");
				metadata.put("docid", file.getName());
				metadata.put("entities", "entities");
				metadata.put("entitiesFile", entitiesPath+file.getName());
				boolean addedDocument = engine.addDocument(text, metadata);
				if(addedDocument) {
					System.out.println("ADDED.");
				}
				else {
					System.out.println("ERROR.");
				}
				cnt++;
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			System.exit(0);
		}
	}
	
	
	@Operation(hidden=true)
	@RequestMapping(value = "/testURL", method = { RequestMethod.POST, RequestMethod.GET })
	public ResponseEntity<String> testURL(
			@RequestParam(value = "preffix", required = false) String preffix,
			@RequestBody(required = false) String postBody) throws Exception {
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/plain");
		ResponseEntity<String> response = new ResponseEntity<String>("The restcontroller is working properly", responseHeaders, HttpStatus.OK);
		return response;
	}

	@Operation(
			//            security = {@SecurityRequirement(name = "authorizationCode")},
			summary = "Adds a document to the search index for the text-qa module.",
			responses = {
					@ApiResponse(responseCode = "200",
							description = "Message stating that the document has been correctly added.",
							content = @Content(mediaType = MediaType.TEXT_PLAIN_VALUE)),
					@ApiResponse(responseCode = "400",
					description = "Bad request. Body cannot be null.",
					content = @Content(mediaType = MediaType.TEXT_PLAIN_VALUE)),
					@ApiResponse(responseCode = "500",
					description = "An error has ocurred in the server.",
					content = @Content(mediaType = MediaType.TEXT_PLAIN_VALUE))
			})
	@CrossOrigin
	@RequestMapping(value = "/addDocument",
	method = {	RequestMethod.POST },
	consumes = {MediaType.APPLICATION_JSON_VALUE},
	produces = {MediaType.APPLICATION_JSON_VALUE}
			)
	public ResponseEntity<String> addDocument(
			HttpServletRequest request,
			@RequestParam(value = "language", required = false) String language,
			@RequestHeader(value = "Accept", required = false) String acceptHeader,
			@RequestHeader(value = "Content-Type", required = false) String contentTypeHeader,
			@RequestParam Map<String, String> allParams,
			@RequestBody(required = false) String postBody) throws Exception {
		if (postBody == null) {
			String msg = "Body of POST call cannot be empty or null.";
			logger.error("Exception: "+msg);
			throw new Exception(msg);
		}
		JSONObject jsonBody = new JSONObject(postBody);
		String text = jsonBody.getString("text");
		HashMap<String,String> metadata = new ObjectMapper().readValue(jsonBody.getJSONObject("metadata").toString(), new TypeReference<HashMap<String, String>>(){});
		try {
			HttpHeaders responseHeaders = new HttpHeaders();
			responseHeaders.add("Content-Type", "text/plain");
			// trainingData is now expected to be a String (containing the annotated data). Perhaps we want to change this to accept a file at some point, since annotated data can grow large.
			boolean addedDocument = engine.addDocument(text, metadata);
			String body = "ERROR, the document has not been correctly added.";
			if(addedDocument) {
				body = "The document has been correctly added.";
			}
			ResponseEntity<String> response = new ResponseEntity<String>(body, responseHeaders, HttpStatus.OK);
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	@Operation(
			security = {@SecurityRequirement(name = "authorizationCode")},
			summary = "Searches for answer to a question.",
			responses = {
					@ApiResponse(responseCode = "200",
							description = "JSONObject containing the answers and related information.",
							content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE)),
					@ApiResponse(responseCode = "500",
					description = "An error has ocurred in the server.",
					content = @Content(mediaType = MediaType.TEXT_PLAIN_VALUE))
			})
	@CrossOrigin
	@RequestMapping(value = "/search",
	method = {	RequestMethod.POST },
	consumes = {MediaType.TEXT_PLAIN_VALUE, MediaType.APPLICATION_JSON_VALUE},
	produces = {MediaType.APPLICATION_JSON_VALUE}
			)
	public ResponseEntity<String> search(
			HttpServletRequest request,
			//			@Parameter(description = "The annotation model to be used for the enrichment.") @RequestParam(value = "models", required = false) String models,
			@RequestParam(value = "language", required = false) String language,
			@RequestParam(value = "query", required = false) String query,
			@Parameter(hidden=true) @RequestHeader(value = "Accept", required = false) String acceptHeader,
			@Parameter(hidden=true) @RequestHeader(value = "Content-Type", required = false) String contentTypeHeader,
			//			@Parameter(hidden=true) @RequestParam(value = "content", required = false, defaultValue = "true") boolean isContent,
			//			@Parameter(hidden=true) @RequestParam(value = "outputCallback", required = false) String outputCallback,
			//			@Parameter(hidden=true) @RequestParam(value = "statusCallback", required = false) String statusCallback,
			@io.swagger.v3.oas.annotations.parameters.RequestBody(description = "The Lynx document.") @RequestBody(required = false) String postBody) throws Exception {
		if (language == null || language.equalsIgnoreCase("")) {
			language = "en";
			logger.warn("The input language is not defined. English (\"EN\") is used.");
		}

		if(query!=null && !query.isEmpty()) {			
		}
		else {
			if (postBody == null || postBody.equalsIgnoreCase("")) {
				logger.error("Error: body can not be null or empty. It must contain data for processing or document URL.");
				throw new Exception("Error: body can not be null or empty. It must contain data for processing or document URL.");
			}
			else {
				if(contentTypeHeader.equalsIgnoreCase("text/plain")) {
					query = postBody;
				}
				else if(contentTypeHeader.equalsIgnoreCase("application/json")) {
					JSONObject jBody = new JSONObject(postBody);
					query = jBody.getString("content");
				}
				else {
					String msg = "The Content-Type is not supported. 'text/plain' or 'application/json' including the query as 'text' key must be used.";
					logger.error(msg);
					throw new Exception(msg);
				}
			}
		}
		if (acceptHeader == null || acceptHeader.equalsIgnoreCase("")) {
			acceptHeader = "application/json";
			logger.warn("The AcceptHeader is not defined. 'application/json' is used.");
		}

		try {
			HttpHeaders responseHeaders = new HttpHeaders();
			HttpStatus status = null;
			responseHeaders.add("Content-Type", "text/plain");
			responseHeaders.add("Content-Type", "application/json");
			status = HttpStatus.INTERNAL_SERVER_ERROR;
			status = HttpStatus.OK;

			List<Answer> answers = engine.search(query);
			JSONObject result = new JSONObject();
			result.put("query", query);
			JSONArray array = new JSONArray();
			
			for (Answer answer : answers) {
				array.put(answer.toELGJSON());
			}
			
			JSONObject jResponse = new JSONObject();
			JSONObject jResponse2 = new JSONObject();
			jResponse2.put("type", "texts");
			jResponse2.put("warnings", new JSONArray());
			jResponse2.put("texts", array);
			jResponse.put("response", jResponse2);
			ResponseEntity<String> response = new ResponseEntity<String>(jResponse.toString(), responseHeaders, status);
			return response;

			//			String documentURI = textForProcessing;
			//            HttpHeaders responseHeaders = new HttpHeaders();
			//            HttpStatus status = null;
			//			if(outputCallback==null || outputCallback.equalsIgnoreCase("")) {
			//	    		String result = service.analyzeSynchronous(textForProcessing, contentTypeHeader, acceptHeader, prefix, analysis, models, mode, language, isContent, outputCallback);
			//	    		if(result==null) {
			//	    			String msg = "ERROR: NULL response from AnalyzeSynchronous.";
			//		        	logger.error(msg);
			//		            responseHeaders.add("Content-Type", "text/plain");
			//					status = HttpStatus.INTERNAL_SERVER_ERROR;
			//					result = msg;
			//	    		}
			//	    		else {
			//	    			responseHeaders.add("Content-Type", acceptHeader);
			//	    			status = HttpStatus.OK;
			//	    		}
			//	    		ResponseEntity<String> response = new ResponseEntity<String>(result, responseHeaders, status);
			//	            return response;
			//			}
			//			else {
			//	    		String result = service.analyzeAsynchronous(textForProcessing, contentTypeHeader, acceptHeader, prefix, analysis, models, mode, language, isContent, outputCallback);
			//                responseHeaders.add("Content-Type", acceptHeader);
			//	    		ResponseEntity<String> response = new ResponseEntity<String>(result, responseHeaders, HttpStatus.ACCEPTED);
			//	            return response;
			//			}

		} catch (Exception e) {
			logger.error(e.getMessage());
			String result = "An Exception occured during execution. The message is: "+e.getMessage();
			HttpHeaders responseHeaders = new HttpHeaders();
			responseHeaders.add("Content-Type", acceptHeader);
			ResponseEntity<String> response = new ResponseEntity<String>(result, responseHeaders, HttpStatus.INTERNAL_SERVER_ERROR);
			return response;
		}
	}

	@Operation(
			//            security = {@SecurityRequirement(name = "authorizationCode")},
			summary = "Adds a document to the search index for the text-qa module.",
			responses = {
					@ApiResponse(responseCode = "200",
							description = "Message stating that the document has been correctly added.",
							content = @Content(mediaType = MediaType.TEXT_PLAIN_VALUE)),
					@ApiResponse(responseCode = "400",
					description = "Bad request. Body cannot be null.",
					content = @Content(mediaType = MediaType.TEXT_PLAIN_VALUE)),
					@ApiResponse(responseCode = "500",
					description = "An error has ocurred in the server.",
					content = @Content(mediaType = MediaType.TEXT_PLAIN_VALUE))
			})
	@CrossOrigin
	@RequestMapping(value = "/addPredefinedCollection",
	method = {	RequestMethod.POST },
	consumes = {MediaType.APPLICATION_JSON_VALUE},
	produces = {MediaType.APPLICATION_JSON_VALUE}
			)
	public ResponseEntity<String> addPredefinedCollection(
			HttpServletRequest request,
			@RequestHeader(value = "Accept", required = false) String acceptHeader,
			@RequestHeader(value = "Content-Type", required = false) String contentTypeHeader,
			@RequestParam Map<String, String> allParams,
			@RequestBody(required = false) String postBody) throws Exception {
		if (postBody == null) {
			String msg = "Body of POST call cannot be empty or null.";
			logger.error("Exception: "+msg);
			throw new Exception(msg);
		}
		JSONObject jsonBody = new JSONObject(postBody);
		String name = jsonBody.getString("collectionName");
		try {
			HttpHeaders responseHeaders = new HttpHeaders();
			responseHeaders.add("Content-Type", "application/json");
			// trainingData is now expected to be a String (containing the annotated data). Perhaps we want to change this to accept a file at some point, since annotated data can grow large.
			
			String collectionPath = "src/main/resources/collections/"+name+"/";
			File folder = new File(collectionPath);
			File [] files = folder.listFiles();
			JSONArray errors = new JSONArray();
			JSONArray successes = new JSONArray();
			int cnt = 1;
			System.out.println(folder.getAbsolutePath());
			for (File file : files) {
				System.out.println(file.getName());
				String text = FileUtils.readFileToString(file,"utf-8");
				HashMap<String,String> metadata = new HashMap<String, String>();
				metadata.put("id", cnt+"");
				metadata.put("docid", file.getName());
				metadata.put("entities", "entities");
				boolean addedDocument = engine.addDocument(text, metadata);
				if(addedDocument) {
					successes.put(file.getName());
				}
				else {
					errors.put(file.getName());
				}
				cnt++;
			}
			JSONObject json = new JSONObject();
			json.put("successes", successes);
			json.put("errors", errors);
			ResponseEntity<String> response = new ResponseEntity<String>(json.toString(), responseHeaders, HttpStatus.OK);
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}
	
}


