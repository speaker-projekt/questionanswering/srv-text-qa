package de.dfki.speaker.qa.engine.searchengine;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import de.dfki.speaker.qa.engine.questionprocessing.ProcessedQuestion;

public interface Searcher {

	Logger logger = Logger.getLogger(Searcher.class);

	public List<SearchDocument> searchDocuments(ProcessedQuestion processedQuestion, List<SearchDocument> resultList) throws Exception;

	public boolean addDocuments(List<String> text, List<Map<String, String>> metadatas) throws Exception;

	public boolean addDocument(String text, Map<String, String> metadata) throws Exception;

}
