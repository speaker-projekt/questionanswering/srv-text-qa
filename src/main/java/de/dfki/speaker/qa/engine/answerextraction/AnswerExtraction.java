package de.dfki.speaker.qa.engine.answerextraction;

import java.util.LinkedList;
import java.util.List;

import org.springframework.stereotype.Component;

import de.dfki.speaker.qa.engine.questionprocessing.ProcessedQuestion;
import de.dfki.speaker.qa.engine.searchengine.SearchDocument;

@Component
public class AnswerExtraction {

	List<AnswerExtractor> extractors;
	
	public AnswerExtraction(){
		extractors = new LinkedList<AnswerExtractor>();
//		extractors.add(new TFIDFAnswerExtractor());
		extractors.add(new WholeDocumentAnswerExtractor());
	}
	
	public List<Answer> extractAnswers(ProcessedQuestion pq, List<SearchDocument> documents) {
		List<Answer> answers = new LinkedList<Answer>();
		/**
		 * Process question and include relevant information in the PQ object.
		 */
		for (AnswerExtractor ae : extractors) {
			ae.extractAnswers(pq, documents, answers);
		}
		return answers;
	}

}
