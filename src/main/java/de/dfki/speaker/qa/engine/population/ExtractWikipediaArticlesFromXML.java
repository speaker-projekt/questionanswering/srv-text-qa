package de.dfki.speaker.qa.engine.population;

import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.io.FileUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class ExtractWikipediaArticlesFromXML {

	public static void main(String[] args) throws Exception {
		
		String path = "src/main/resources/wikipedia_short_2/";
		
		System.out.print("Loading XML file...");
	    File folder = new File(path);
	    File files[] = folder.listFiles();
	    
	    for (File file : files) {
			String content = FileUtils.readFileToString(file, "utf-8");
			
		}

	}
}
