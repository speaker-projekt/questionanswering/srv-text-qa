package de.dfki.speaker.qa.engine.questionprocessing;

public class QuestionTypeProcessor implements QuestionProcessor{

	public void processQuestion(ProcessedQuestion pq) {
		String qT = null;
		String questionText = pq.getText().toLowerCase().trim();
		
		if(questionText.startsWith("what")) {
			qT = qT + "what";
		}
		else if(questionText.startsWith("which")) {
			qT = qT + "which";
		}
		else if(questionText.startsWith("how")) {
			qT = qT + "how";
		}
		else if(questionText.startsWith("who")) {
			qT = qT + "who";
		}
		else if(questionText.startsWith("why")) {
			qT = qT + "why";
		}
		else if(questionText.startsWith("when")) {
			qT = qT + "when";
		}
		else {
			qT = "other";
		}
		pq.setQuestionType(qT);
	}
	
	
}
