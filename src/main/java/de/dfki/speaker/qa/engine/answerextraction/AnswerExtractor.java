package de.dfki.speaker.qa.engine.answerextraction;

import java.util.List;

import de.dfki.speaker.qa.engine.questionprocessing.ProcessedQuestion;
import de.dfki.speaker.qa.engine.searchengine.SearchDocument;

public interface AnswerExtractor {

	public List<Answer> extractAnswers(ProcessedQuestion pq, List<SearchDocument> documents, List<Answer> answers);

}
