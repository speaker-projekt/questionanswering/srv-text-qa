package de.dfki.speaker.qa.engine.questionprocessing;

public interface QuestionProcessor {

	public void processQuestion(ProcessedQuestion pq);	
	
}
