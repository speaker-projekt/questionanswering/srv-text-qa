package de.dfki.speaker.qa.engine.searchengine.lucene.indexmanagement.queryparser;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.core.WhitespaceAnalyzer;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.BooleanQuery.Builder;
import org.apache.lucene.search.Query;

import de.dfki.speaker.qa.engine.searchengine.lucene.indexmanagement.analyzer.AnalyzerFactory;


public class OwnQueryParser implements IQueryParser{
	
	public static Query parseDocumentIdQuery(String documentId) throws Exception {
		try{
			QueryParser parser1 = new QueryParser("documentId", new WhitespaceAnalyzer());
			Query query1 = parser1.parse(scapeStringForLuceneQuery(documentId));
			return query1;
		}
		catch(Exception e){
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
	}
	
	public static Query parseQuery(String queryContent, String[] fields, String [] analyzers, String language) throws Exception {
		Builder buil = new Builder();
		try{
			String queryString = queryContent;
			if(fields==null || fields.length==0 || (fields.length==1 && fields[0].equalsIgnoreCase("all")) ){
				fields = new String []{"content","entities","links","temporal","nifcontent","docURI"};
				analyzers = new String []{"standard","standard","standard","standard","standard","standard"};
			}
			for (int i = 0; i < fields.length; i++) {
				Analyzer particularAnalyzer = AnalyzerFactory.getAnalyzer(analyzers[i],language);
				QueryParser parser1 = new QueryParser(fields[i], particularAnalyzer);
				Query query1 = parser1.parse(queryString);
				buil.add(query1, BooleanClause.Occur.SHOULD);
			}
		}
		catch(Exception e){
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
		BooleanQuery booleanQuery = buil.build();
		return booleanQuery;
	}

	public static Query parseComplexQuery(String queryType, String queryContent, String[] fields, String [] analyzers, String language) throws Exception{
		Builder buil = new Builder();
		try{
			if(queryType.equalsIgnoreCase("docid") || queryType.equalsIgnoreCase("tfidf") || queryType.equalsIgnoreCase("plaintext") ){
				String queryString = queryContent;
				if(fields.length==1){
					String field = (queryType.equalsIgnoreCase("plaintext")) ? "content" : fields[0];
					Analyzer analyzer = AnalyzerFactory.getAnalyzer(analyzers[0], language);
					QueryParser parser1 = new QueryParser(field, analyzer);
					Query query1 = parser1.parse(queryString);
					buil.add(query1, BooleanClause.Occur.SHOULD);
				}
				else{
					for (int i = 0; i < fields.length; i++) {
						Analyzer particularAnalyzer = AnalyzerFactory.getAnalyzer(analyzers[i],language);
						QueryParser parser1 = new QueryParser(fields[i], particularAnalyzer);
						Query query1 = parser1.parse(queryString);
						buil.add(query1, BooleanClause.Occur.SHOULD);
					}
				}
			}
			else{
				throw new Exception();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
		BooleanQuery booleanQuery = buil.build();
		return booleanQuery;
	}

	public static String scapeStringForLuceneQuery(String s){
		s = s.replaceAll("\\\\", "\\\\\\\\");
		s = s.replaceAll("\\+", "\\\\\\+");
		s = s.replaceAll("\\-", "\\\\\\-");
		s = s.replaceAll("&", "\\\\&");
		s = s.replaceAll("\\|", "\\\\\\|");
		s = s.replaceAll("!", "\\\\!");
		s = s.replaceAll("\\(", "\\\\\\(");
		s = s.replaceAll("\\)", "\\\\\\)");
		s = s.replaceAll("\\[", "\\\\\\[");
		s = s.replaceAll("\\]", "\\\\\\]");
		s = s.replaceAll("\\{", "\\\\\\{");
		s = s.replaceAll("\\}", "\\\\\\}");
		s = s.replaceAll("\\^", "\\\\\\^");
		s = s.replaceAll("\"", "\\\\\"");
		s = s.replaceAll("~", "\\\\~");
		s = s.replaceAll("\\*", "\\\\\\*");
		s = s.replaceAll("\\?", "\\\\\\?");
		s = s.replaceAll(":", "\\\\:");
		s = s.replaceAll("/", "\\\\/");
		return s;
	}
}
