package de.dfki.speaker.qa.engine.searchengine.lucene.indexmanagement.documentparser;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;

import edu.stanford.nlp.pipeline.CoreDocument;
import edu.stanford.nlp.pipeline.CoreEntityMention;
import edu.stanford.nlp.pipeline.CoreSentence;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;

/**
 * @author Julian Moreno Schneider julian.moreno_schneider@dfki.de
 * @modified_by 
 * @project srv-text-qa
 * @date 23.12.2020
 * @date_modified 
 * @company DFKI
 * @description 
 * Class that generates a Lucene Document from a TXT file. The lucene document contains three fields:
 * 	- Title: the first line of the TXT file.
 *  - Body: the rest of the TXT file.
 *  - Entities: the whole text = title + body.
 *  - Path: the name of the file.
 *
 */
public class TXTDocumentParser implements IDocumentParser{

	public TXTDocumentParser() {
	}

	@Override
	public Document parseDocumentFromFile(String path, String[] fields) {
		try{
			//For spring approach the next lines must be used
			//ClassPathResource cpr = new ClassPathResource("classpath:"+path);
			//BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(cpr.getFile()), "utf-8"));
			Document doc = new Document();
			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(path), "utf-8"));
			String title = br.readLine();
			String line = br.readLine();
			String body = null;
			while(line!=null){
				if(body==null)
					body = line;
				else
					body=body + " " + line;
				line = br.readLine();
			}
			br.close();
			doc.add(new TextField("title", title, Field.Store.YES));
			doc.add(new TextField("content", body, Field.Store.YES));
			doc.add(new TextField("entities", title + " " + body, Field.Store.NO));
			doc.add(new StringField("path", path, Field.Store.YES));

			// Note that FileReader expects the file to be in UTF-8 encoding. If that's not the case searching for special characters will fail.
//			doc.add(new TextField("contents", new BufferedReader(new InputStreamReader(stream, StandardCharsets.UTF_8))));
			
			return doc;
		}
		catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public Document parseDocumentFromString(String content, String[] fields) {
		Document doc = new Document();
		doc.add(new TextField("id", content, Field.Store.YES));
		doc.add(new TextField("title", content, Field.Store.YES));
		doc.add(new TextField("text", content, Field.Store.YES));
		doc.add(new TextField("entities", content, Field.Store.YES));
		// Note that FileReader expects the file to be in UTF-8 encoding. If that's not the case searching for special characters will fail.
//		doc.add(new TextField("contents", new BufferedReader(new InputStreamReader(stream, StandardCharsets.UTF_8))));
		
		return doc;	
	}

	@Override
	public Document parseDocumentFromString(String content, Map<String, String> metadata, String[] fields) {
		if(metadata==null || metadata.isEmpty()) {
			return parseDocumentFromString(content, fields);
		}
		Document doc = new Document();
		if(metadata.containsKey("id") && metadata.get("id")!=null && !metadata.get("id").equalsIgnoreCase("")) {
			doc.add(new TextField("id", metadata.get("id"), Field.Store.YES));
		}
		doc.add(new TextField("text", content, Field.Store.YES));
		if(metadata.containsKey("entities") && metadata.get("entities")!=null && !metadata.get("entities").equalsIgnoreCase("")) {
			doc.add(new TextField("entities", metadata.get("entities"), Field.Store.YES));
		}
		return doc;	
	}

	@Override
	public List<Document> parseSentenceDocumentsFromString(String docContent, Map<String, String> metadata, String[] fields, StanfordCoreNLP pipeline){
		List<Document> documents = new LinkedList<Document>();
		if(metadata==null || metadata.isEmpty()) {
			metadata.put("id", (new Date()).getTime()+"");
		}
		String docId = "";
		if(metadata.containsKey("id") && metadata.get("id")!=null && !metadata.get("id").equalsIgnoreCase("")) {
			docId = metadata.get("id");
		}
		else {
			docId = (new Date()).getTime()+"";
		}

		String listEntities [] = null;
		List<String> listEntities2 = null;
		try {
			String entitiesContent = FileUtils.readFileToString(new File(metadata.get("entitiesFile")), "utf-8");
			listEntities = entitiesContent.split("\n");
			listEntities2 = FileUtils.readLines(new File(metadata.get("entitiesFile")), "utf-8");
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		
//		System.out.println(docContent.substring(0,100));
		List<String> sentences= extractAndCleanLines(docContent);
//		System.out.println("Contains "+sentences.size()+" sentences.");

		if(sentences.size()!=listEntities2.size()) {
			System.out.println("ERROR. ERROR.ERROR. Sizes do not match.");
			System.out.println(sentences.size());
			System.out.println(listEntities2.size());
			return null;
		}
		int cnt = 0;
		for (int i = 0; i < sentences.size(); i++) {
			cnt++;
//        System.out.println(sent.text());
			String sentence = sentences.get(i);			
//			String sEntities = listEntities[i];
			String sEntities = listEntities2.get(i);
//			CoreDocument cdoc = new CoreDocument(sentence);
//			pipeline.annotate(cdoc);
//			List<CoreEntityMention> entities = cdoc.entityMentions();
//			String sEntities = "";
//			if(entities!=null) {
//				for (CoreEntityMention ent : entities) {
//					sEntities = sEntities + ";" + ent.text();
//				}
//			}
			Document doc = new Document();
			doc.add(new TextField("id", docId+"_"+cnt, Field.Store.YES));
			doc.add(new TextField("docid", docId, Field.Store.YES));
			doc.add(new TextField("text", sentence, Field.Store.YES));
			doc.add(new TextField("entities", sEntities, Field.Store.YES));
//			System.out.println(doc.toString());
			documents.add(doc);
		}
		return documents;
	}
	
	public static void main(String[] args) throws Exception {
		
		StanfordCoreNLP pipeline;

		Properties props = new Properties();
		props.setProperty("annotators", "tokenize,ssplit,pos,lemma,ner");
		// set up pipeline
		pipeline = new StanfordCoreNLP(props);		

		
		String outputfolder = "src/main/resources/collections/wikipedia_short_sentences_entities/";
		File folder = new File("src/main/resources/collections/wikipedia_short_sentences/");
		File filess[] = folder.listFiles();
		for (File file : filess) {
			BufferedWriter bw = new BufferedWriter(new FileWriter(outputfolder+file.getName()));
//			File file = new File("src/main/resources/collections/wikipedia_short_sentences/Austin_Powers_(character).txt");
			System.out.println("\n\n\n\n\n---------------------------------------------");
			String content = FileUtils.readFileToString(file, "utf-8");
			TXTDocumentParser t = new TXTDocumentParser();
			List<String> liness = t.extractAndCleanLines(content);
			for (String line : liness) {
				System.out.println(line);
				CoreDocument cdoc = new CoreDocument(line);
				pipeline.annotate(cdoc);
		        List<CoreEntityMention> entities = cdoc.entityMentions();
		        String sEntities = "";
		        for (CoreEntityMention ent : entities) {
					sEntities = sEntities + ";" + ent.text();
				}
		        bw.write(sEntities+"\n");
		        System.out.println("------ ENTITIES: " + sEntities);
			}
			bw.close();
		}		
	}
	
	public List<String> extractAndCleanLines(String content) {
		List<String> outputLines = new LinkedList<String>();
		try {
			String lines[] = content.split("\n");
			boolean insideBrackets = false;
			for (String line : lines) {
				if(insideBrackets) {
					if(line.trim().equalsIgnoreCase("}}")) {
						insideBrackets = false;
					}
					else {
					}
				}
				else if(line.trim().startsWith("{{")) {
					if(line.trim().contains("}}")) {
					}
					else {
						insideBrackets = true;
					}
				} else 
					if(line.trim().equalsIgnoreCase("") || 
						line.trim().startsWith("==") || 
						line.trim().toLowerCase().startsWith("category") || 
						line.trim().toLowerCase().startsWith("* {{") || 
						line.trim().toLowerCase().startsWith("|") || 
						line.trim().toLowerCase().startsWith("file")) {
				}
				else {
	//				System.out.println(line);
					outputLines.add(line);
				}
			}
			return outputLines;
		}
		catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
