package de.dfki.speaker.qa.engine.searchengine.lucene.indexmanagement.documentparser;

import java.util.List;
import java.util.Map;

import org.apache.lucene.document.Document;

import edu.stanford.nlp.pipeline.StanfordCoreNLP;

/**
 * @author Julian Moreno Schneider julian.moreno_schneider@dfki.de
 * @modified_by 
 * @project srv-entity-linking
 * @date 24.06.2020
 * @date_modified 
 * @company DFKI
 * @description 
 * Class that generates a Lucene Document from a TXT file. The lucene document contains three fields:
 * 	- Title: the first line of the TXT file.
 *  - Body: the rest of the TXT file.
 *  - Entities: the whole text = title + body.
 *
 */
public interface IDocumentParser {

	public Document parseDocumentFromFile (String path, String[] fields);

	public Document parseDocumentFromString (String content, String[] fields);

	public Document parseDocumentFromString(String content, Map<String, String> metadata, String[] fields);

	public List<Document> parseSentenceDocumentsFromString(String docContent, Map<String, String> metadata, String[] fields, StanfordCoreNLP pipeline);

}