package de.dfki.speaker.qa.engine.searchengine;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import de.dfki.speaker.qa.engine.questionprocessing.ProcessedQuestion;

@Component
public class SearchEngine {

	Logger logger = Logger.getLogger(SearchEngine.class);

	List<Searcher> searchers;
	
	@Autowired
	private Environment env;

	@Value("${app.isDemo}")
	boolean isDemo = true;
	
	@Value("${lucene.indexType}")
	String indexType;
	
	@Value("${lucene.search.language}")
	String language;
	
	@Value("${lucene.search.fields}")
	String fields;
	
	@Value("${lucene.search.analyzers}")
	String analyzers;
	
	@Value("${lucene.search.indexName}")
	String indexName;
	
	public SearchEngine(){
	}
	
	@PostConstruct
	public void setUp() {
		System.out.println("DEBUG: demo or not: "+isDemo);
		System.out.println("DEBUG: language: "+language);
		searchers = new LinkedList<Searcher>();
		if(isDemo) {
			try {
				if(indexType.equalsIgnoreCase("sentence")) {
					System.out.println("SENTENCE INDEX");
					searchers.add(new LuceneSentenceSearcher(indexName, language, fields, analyzers, false));
				}
				else {
					System.out.println("DOCUMENT INDEX");
					searchers.add(new LuceneSearcher(indexName, language, fields, analyzers, false));
				}
			} catch (Exception e) {
				e.printStackTrace();
				logger.warn("Error thrown when creating LuceneSearcher");
			}
		}
		else {
//			searchers.add(new SolrSearcher());
			try {
				searchers.add(new LuceneSearcher());
			} catch (Exception e) {
				e.printStackTrace();
				logger.warn("Error thrown when creating LuceneSearcher");
			}
		}
	}
	
	public List<SearchDocument> searchDocuments(ProcessedQuestion processedQuestion) throws Exception {
		List<SearchDocument> resultList = new LinkedList<SearchDocument>();
		/**
		 * Process question and include relevant information in the PQ object.
		 */
		for (Searcher s : searchers) {
			s.searchDocuments(processedQuestion, resultList);
		}
		return resultList;
	}

	public boolean addDocument(String text, Map<String, String> metadata) throws Exception {
		/**
		 * Add document to all searchers.
		 */
		for (Searcher s : searchers) {
			s.addDocument(text, metadata);
		}
		return true;
	}

	public boolean addDocuments(List<String> documents, List<Map<String, String>> metadatas) throws Exception {
		/**
		 * Add document to all searchers.
		 */
		for (Searcher s : searchers) {
			s.addDocuments(documents, metadatas);
		}
		return true;
	}

}
