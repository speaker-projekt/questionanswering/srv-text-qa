package de.dfki.speaker.qa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"de.dfki.speaker.qa"})
public class TextQAApplication {

    public static void main(String[] args) {
        SpringApplication.run(TextQAApplication.class, args);
    } 

}
