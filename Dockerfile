FROM maven:3.6-jdk-11-slim

MAINTAINER Julian Moreno Schneider <julian.moreno_schneider@dfki.de>

RUN mkdir /var/maven/ && chmod -R 777 /var/maven
RUN mkdir mkdir /tmp/srv-text-qa/ && chmod -R 777 /tmp/srv-text-qa
ENV MAVEN_CONFIG /var/maven/.m2

ADD pom.xml /tmp/srv-text-qa
#COPY lib /tmp/srv-text-qa/lib
RUN cd /tmp/srv-text-qa && mvn -B -e -C -T 1C -Duser.home=/var/maven org.apache.maven.plugins:maven-dependency-plugin:3.0.2:go-offline 

RUN chmod -R 777 /var/maven
EXPOSE 8088

COPY . /tmp/srv-text-qa
COPY src/main/resources/application_server.properties /tmp/srv-text-qa/src/main/resources/application.properties
WORKDIR /tmp/srv-text-qa
RUN mvn -Duser.home=/var/maven clean install -DskipTests

RUN chmod -R 777 /tmp/srv-text-qa

CMD mvn -Duser.home=/var/maven spring-boot:run
#CMD ["/bin/bash"]
